birthCHN = {}
birthCHN["Doombringer"] = {
	chname = "毁灭使者",
	desc = {
		"	利 刃 在 手 ， 烈 焰 匿 踪 , 毁 灭 使 者 是 战 斗 中 的 强 大 力 量 。 ",
		"	毁 灭 使 者 是 纯 粹 的 战 争 机 器 ， 使 用 利 刃 和 烈 焰 直 接 凿 穿 敌 人 的 军 队 。 ",
		"	最 强 大 的 毁 灭 使 者 可 以 全 面 激 发 恶 魔 血 脉 ， 变 身 成 为 巨 大 的 恶 魔 。 ",
		"他 们 最 重 要 的 属 性 是： 力 量 和 魔 法。",
		"#GOLD#属 性 修 正 ：",
		"#LIGHT_BLUE# * +4 力 量, +0 敏 捷, +2 体 质",
		"#LIGHT_BLUE# * +2 魔 法, +0 意 志, +1 灵 巧",
		"#GOLD#每 等 级 生 命 加 值：#LIGHT_BLUE# +3",
	},
}

birthCHN["Demonologist"] = {
	chname = "恶魔使者",
	locked_desc = "Most simply run, but I understand: a distant planet, ravaged and damned. Burnt creations seek righteous vengeance, Urh'Rok's ashes, now destruction's engines. Harness their power! Capture and tame! I call on you, demons - UNLEASH THE FLAMES!",
	desc = {
		"	不 同 于 众 人 所 相 信 的 ， 恶 魔 使 者 并 非 恶 魔 的 走 卒 。 他 们 只 是 小 心 谨 慎 地 与 恶 魔 签 订 契 约 。",
		"	他 们 利 用 恶 魔 之 力 达 成 所 愿，无 论 善 恶，亦 有 少 数 人 以 此 反 击 恶 魔 。",
		"	恶 魔 使 者 是 近 身 格 斗 的 战 士 ，用 盾 牌 击 碎 敌 人 的 头 颅， 同 时 能 呼 唤 黑 暗 与 火 焰 之 力。",
		"#GOLD#属 性 修 正 ：",
		"#LIGHT_BLUE# * +3 力 量, +0 敏 捷, +2 体 质",
		"#LIGHT_BLUE# * +4 魔 法, +0 意 志, +0 灵 巧",
		"#GOLD#每 等 级 生 命 加 值：#LIGHT_BLUE# +2",
	},

}

birthCHN["Doomelf"] = {
	chname = "魔化精灵",
	locked_desc = "The demons of Mal'Rok would never bless you!\t\nThree could tell them of the horrors elves wrought.\t\nOne rages and torments in deep oceans blue,\t\none fights for the third with the cultists she taught.\t\nSilence these beings, maintain your deception,\t\nand then you may witness a new elf's conception...",
	desc = {
		"魔 化 精 灵 并 不 是 一 个 真 正 的 种 族 ， 他 们 曾 是 永 恒 精 灵 ， 而 被 恶 魔 抓 去， 变 为 末 日 的 使 者。",
		"他 们 对 折 磨 敌 人 感 到 享 受。",
		"他 们 具 有#GOLD# 加 速 #WHITE# 技 能，能 每 隔 一 段 时  间 让 自 己 脱 离 相  位 空 间。",
		"#GOLD#属 性 修 正 ：",
		"#LIGHT_BLUE# * -2 力 量, +1 敏 捷, +1 体 质",
		"#LIGHT_BLUE# * +3 魔 法, +2 意 志, +0 灵 巧",
		"#GOLD#每 等 级 生 命 加 值：#LIGHT_BLUE# +9",
		"#GOLD#经 验 惩 罚 ： #LIGHT_BLUE# 25%",
	}
}
birthCHN["Stone Warden"] = {
	chname = "岩石守卫",
	desc = {
		"岩 石 守 卫 是 那 些 同 时 训 练 魔 法 技 艺 与 自 然 力 量 的 矮 人。",
		"在 其 他 种 族 受 困 于 自 然 和 魔 法 天 生 对 立 的 偏 见 时 ， 矮 人 们 已 经 找 到 方 法 让 这 两 种 力 量 和 谐 共 存 。",
		"岩 石 守 卫 是 身 着 重 甲 的 战 士 ，双 持 盾 牌 来 施 展 技 艺 。",
		"#GOLD#属 性 修 正 ：",
		"#LIGHT_BLUE# * +2 力 量, +0 敏 捷, +0 体 质",
		"#LIGHT_BLUE# * +4 魔 法, +3 意 志, +0 灵 巧",
		"#GOLD#每 等 级 生 命 加 值：#LIGHT_BLUE# +2",
	},
}
