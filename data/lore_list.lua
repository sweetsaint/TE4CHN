﻿loreList = {}
loreCat = {}

--lore cat
loreCat["age of allure"] = "厄流纪"
loreCat["age of pyre"] = "派尔纪"
loreCat["angolwen"] = "安格利文"
loreCat["arena"] = "竞技场"
loreCat["blighted ruins"] = "荒芜废墟"
loreCat["daikara"] = "岱卡拉"
loreCat["dogroth caldera"] = "达格罗斯火山"
loreCat["dreadfell"] = "恐惧王座"
loreCat["fearscape"] = "恐惧长廊"
loreCat["misc"] = "杂项"
loreCat["high peak"] = "巅峰"
loreCat["ruined dungeon"] = "地城废墟"
loreCat["infinite dungeon"] = "无尽地下城"
loreCat["iron throne"] = "钢铁王座"
loreCat["kor'pul"] = "卡·普尔"
loreCat["last hope"] = "最后的希望"
loreCat["southspar"] = "南晶岛"
loreCat["last hope graveyard"] = "最后的希望墓地"
loreCat["maze"] = "迷宫"
loreCat["temple of creation"] = "造物者神庙"
loreCat["old forest"] = "古老森林"
loreCat["orc prides"] = "兽人部落"
loreCat["rhaloren"] = "罗兰精灵"
loreCat["sandworm lair"] = "沙虫巢穴"
loreCat["scintillating caves"] = "闪光洞穴"
loreCat["sher'tul"] = "夏·图尔"
loreCat["slazish fens"] = "斯拉伊什沼泽"
loreCat["Spellblaze"] = "魔法大爆炸"
loreCat["age of dusk"] = "黄昏纪"
loreCat["history of the Sunwall"] = "太阳堡垒的日记"
loreCat["trollmire"] = "食人魔沼泽"
loreCat["zigur"] = "伊格"
loreCat["temple of creation"] = "造物者神庙"
loreCat["adventures"] = "冒险家"
loreCat["myths of creation"] = "冒险家"
loreCat["eyal"] = "埃亚尔"
loreCat["ancient elven ruins"] = "古代精灵废墟"
loreCat["races"] = "种族"
loreCat["shatur"] = "夏特尔"
loreCat["artifacts"] = "手工艺品"
loreCat["boss"] = "BOSS"
loreCat["lake of nur"] = "纳尔湖"
loreCat["magic"] = "魔法"
loreCat["dreamscape"] = "梦境空间"
loreCat["keepsake"] = "往昔信物"
--lore list

--age-allure
loreList["research log of halfling mage Hompalan"] =  "半身人法师红帕兰的研究"
loreList["order for director Hompalan"] = "致半身人法师红帕兰的绝密命令"
loreList["healer Astelrid log 1"] = "孔克雷夫医师亚斯特莉的日志 第一部分"
loreList["healer Astelrid log 2"] = "孔克雷夫医师亚斯特莉的日志 第二部分"
loreList["healer Astelrid log 3"] = "孔克雷夫医师亚斯特莉的日志 第三部分"
loreList["healer Astelrid log 4"] = "孔克雷夫医师亚斯特莉的日志 第四部分"

--age-pyre
loreList["Atamathon, the giant golem"] =  "阿塔玛森·傀儡之王"

--angolwen
loreList["Lecture on Humility by Archmage Linaniil"] =  "大法师莱娜尼尔关于谦卑的演讲"
loreList["Lecture on the nature of magic by Archmage Tarelion"] =  "大法师泰尔兰关于魔法本质的演讲"
loreList["The spellblade"] =  "魔法大爆炸"
loreList["Angolwen Fountain"] = "安格列文的喷泉"

--ardhungol
loreList["Rashim Journal (1)"] = "拉希姆的日记(1)"
loreList["Rashim Journal (2)"] = "拉希姆的日记(2)"
loreList["Rashim Journal (3)"] = "拉希姆的日记(3)"

--arena
loreList["Arena for dummies"] =  "竞技场傻瓜指南"

--blighted-ruins
loreList["note from the Necromancer"] =  "死灵法师的笔记"

--daikara
loreList["expedition journal entry (daikara)"] =  "远征队日记（岱卡拉）"

--dreadfell
loreList["note from the Master"] =  "主人的笔记"
loreList["A smudged poem chalked on a dark piece of slate"] =  "一首写在石板上的被弄脏的诗歌"
loreList["A poem written in scribbled, child-like writing"] =  "像小孩子写的潦草诗歌"
loreList["A poem written in an elegant Gothic script"] =  "高雅的哥特体诗歌"
loreList["a note about undead poetry from the Master"] =  "主人关于不死族的笔记"
loreList["slain master"] =  "杀死主人"
loreList["a letter to Borfast from the Master"] = "主人写给波法斯特的信"
loreList["a letter to Aletta from the Master"] = "主人写给阿蕾塔的信"
loreList["a letter to Filio from the Master"] = "主人写给菲里奥的信"

--elvala
loreList["The Spellblaze Chronicles(1): A Fateful Meeting"] = "魔法大爆炸纪事(1)：命运相遇的瞬间"
loreList["The Spellblaze Chronicles(2): A Night to Remember"] = "魔法大爆炸纪事(2)：难忘之夜"

--fearscape
loreList["a fearsome sight"] =  "一副恐怖的景象"
loreList["sacrificial altar"] =  "献祭祭坛"

--fun
loreList["trollish poem"] =  "巨魔之歌"
loreList["necromancer poem"] =  "死灵法师颂"
loreList["how to be a necromancer, part 1"] =  "怎样成为死灵法师　第一章"
loreList["how to be a necromancer, part 2"] =  "怎样成为死灵法师　第二章"
loreList["how to be a necromancer, part 3"] =  "怎样成为死灵法师　第三章"
loreList["how to be a necromancer, part 4"] =  "怎样成为死灵法师　第四章"
loreList["of halfling feet"] =  "半身人的脚"
loreList["Rogues do it from behind"] =  "盗贼在你身后"
loreList["Dust to Dust"] =  "尘归尘"

--high-peak
loreList["closing the void farportal"] =  "关闭虚空传送门"

--infinite-dungeon
loreList["clue (ruined dungeon)"] =  "线索（地下城废墟）"
loreList["infinite dungeon (ruined dungeon)"] =  "无尽地下城（地下城废墟）"
loreList["The Hunter and the Hunted chapter 1"] =  "猎人与猎物　第一章"
loreList["The Hunter and the Hunted chapter 2"] =  "猎人与猎物　第二章"
loreList["The Hunter and the Hunted chapter 3"] =  "猎人与猎物　第三章"
loreList["The Hunter and the Hunted chapter 4"] =  "猎人与猎物　第四章"
loreList["The Hunter and the Hunted chapter 5"] =  "猎人与猎物　第五章"

--iron-throne
loreList["Iron Throne Profits History: Age of Allure"] =  "钢铁王座的盈利历史　厄流纪"
loreList["Iron Throne Profits History: Age of Dusk"] =  "钢铁王座的盈利历史　黄昏纪"
loreList["Iron Throne Profits History: Age of Pyre"] =  "钢铁王座的盈利历史　派尔纪"
loreList["Iron Throne Profits History: Age of Ascendancy"] =  "钢铁王座的盈利历史　卓越纪"
loreList["Iron Throne Edict"] =  "钢铁王座布告"
loreList["Iron Throne trade ledger"] =  "钢铁王座交易总账"
loreList["Iron Throne Reknor expedition, last words"] =  "钢铁王座瑞库纳远征军的遗言"
loreList["Deep Bellow excavation report 1"] =  "无尽深渊挖掘报告1"
loreList["Deep Bellow excavation report 2"] =  "无尽深渊挖掘报告2"
loreList["Deep Bellow excavation report 3"] =  "无尽深渊挖掘报告3"

--keepsake
loreList["A Tranquil Meadow"] = "宁静的草原"
loreList["A Haunting Dream"] = "噩梦"
loreList["Bander's Notes"] = "班德的笔记"
loreList["The Acorn"] = "铁质橡果"
loreList["The Merchant Caravan"] = "商队营地"
loreList["The Dream's End"] = "噩梦初醒"
loreList["The Stone Marker"] = "岩石标记"
loreList["The Sealed Cave"] = "被封印的洞穴"
loreList["The Battle of the Cave"] = "洞穴之战"
loreList["Kyless' Journal: First Entry"] = "克里斯的日记：第一页"
loreList["Kyless' Journal: Second Entry"] = "克里斯的日记：第二页"
loreList["Kyless' Journal: Third Entry"] = "克里斯的日记：第三页"
loreList["Kyless' Journal: Fourth Entry"] = "克里斯的日记：第四页"
loreList["The Vault"] = "藏宝室"
loreList["Kyless"] = "克里斯"
loreList["Keepsake"] = "往昔信物"

--kor-pul
loreList["journal page (kor'pul)"] =  "卡·普尔游记"

--last-hope
loreList["The Diaries of King Toknor the Brave"] =  "勇气之王图库纳的笔记"
loreList["All Hail King Tolak the Fair!"] =  "公正之王托拉克万岁！"
loreList["All Hail King Toknor the Brave!"] =  "勇气之王图库纳万岁！"
loreList["All Hail Queen Mirvenia the Inspirer!"] =  "尊敬的米雯尼雅女王万岁！"
loreList["The Pale King, part one"] =  "亡灵国王，第一章"
loreList["The Pale King, part two"] =  "亡灵国王，第二章"
loreList["Declaration of the Unification of the Allied Kingdoms"] = "联合王国公告"
loreList["The Oceans of Eyal"] = "埃亚尔之海"
loreList["A creased letter"] = "皱巴巴的信"
loreList["gravestone"] =  "墓石"

--maze
loreList["diary (the maze)"] =  "迷宫日记"
loreList["the perfect killing device"] =  "完美的杀人装置"

--misc
loreList["tract of destruction"] = "毁灭之源"
loreList["tract of anarchy"] = "无序之治"
loreList["tract of acceptance"] = "命运抉择"
loreList["personal note (Slasul)"] =  "萨拉苏尔的个人笔记"
loreList["letter to Rolf (1)"] =  "写给罗尔夫的信1"
loreList["letter to Weisman (1)"] =  "写给魏斯曼的信1"
loreList["letter to Rolf (2)"] =  "写给罗尔夫的信2"
loreList["letter to Weisman (2)"] =  "写给魏斯曼的信2"
loreList["letter to Weisman (3)"] =  "写给魏斯曼的信3"
loreList["letter to Rolf (3)"] =  "写给罗尔夫的信3"
loreList["letter to Weisman (4)"] =  "写给魏斯曼的信4"
loreList["Last Will of Rolf"] =  "罗尔夫的遗嘱"
loreList["memories of Artelia Firstborn"] =  "亚特莱长子的记忆"
loreList["human myth of creation"] =  "人类的创世传说"
loreList["a logical analysis of creation, by philosopher Smythen"] =  "创世传说的逻辑分析，哲学家斯迈森著"
loreList["Tale of the Moonsisters"] =  "月亮姐妹的传说"
loreList["ancient papyrus scroll"] =  "古老的莎草卷轴"
loreList["Loremaster Greynot's Analysis of the Races - Introduction"] =  "博学者格雷诺特关于人种的调查——目录"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 1 - Humans"] =  "博学者格雷诺特关于人种的调查——第一章——人类"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 2 - Halflings"] =  "博学者格雷诺特关于人种的调查——第二章——半身人"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 3 - Dwarves"] =  "博学者格雷诺特关于人种的调查——第三章——矮人"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 4 - Shaloren"] =  "博学者格雷诺特关于人种的调查——第四章——永恒精灵"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 5 - Thaloren"] =  "博学者格雷诺特关于人种的调查——第五章——自然精灵"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 6 - Naloren (extinct)"] =  "博学者格雷诺特关于人种的调查——第六章——纳鲁精灵（灭绝）"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 7 - Ogres"] =  "博学者格雷诺特关于人种的调查——第七章——食人魔"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 8 - Orcs (extinct)"] =  "博学者格雷诺特关于人种的调查——第八章——兽人（灭绝）"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 9 - Sher'Tul (extinct)"] =  "博学者格雷诺特关于人种的调查——第九章——夏·图尔人（灭绝）"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 10 - Monstrous Races"] =  "博学者格雷诺特关于人种的调查——第十章——怪物种族"
loreList["Loremaster Greynot's Analysis of the Races - Chapter 11 - Dragons"] =  "博学者格雷诺特关于人种的调查——第十一章——龙族"
loreList["Lament for Lands now Lost"] =  "遗失的大陆的挽歌"
loreList["Running man"] =  "逃跑者"
loreList["Gifts of Nature"] = "自然的恩赐"
loreList["If I Should Die Before I Wake"] = "若醒来前便死去"
loreList["Mocking Note"] = "奚落"
loreList["Nature vs Magic"] = "自然与魔法的对抗"
loreList["On Adventuring"] = "关于冒险"
loreList["Z'quikzshl"] =  "不朽的兹基克茨"
loreList["Walrog"] =  "乌尔罗格"

--noxious-caldera
loreList["tattered paper scrap"] =  "破烂的碎纸片"

--old-forest
loreList["journal entry (old forest)"] =  "古老森林冒险日志"
loreList["magical barrier"] = "魔法屏障"

--orc-prides
loreList["Grushnak's Note"] =  "格鲁希纳克的笔记"
loreList["Rak'Shor's Note"] =  "拉克·肖的笔记"
loreList["Gorbat's Note"] =  "加伯特的笔记"
loreList["Vor's Note"] =  "沃尔的笔记"
loreList["Records of Lorekeeper Hadak part one"] =  "博学者哈达克的记录，第一部分"
loreList["Records of Lorekeeper Hadak part two"] =  "博学者哈达克的记录，第二部分"
loreList["Records of Lorekeeper Hadak part three"] =  "博学者哈达克的记录，第三部分"
loreList["Records of Lorekeeper Hadak part four"] =  "博学者哈达克的记录，第四部分"
loreList["Records of Lorekeeper Hadak part five"] =  "博学者哈达克的记录，第五部分"
loreList["The Legend of Garkul"] =  "加库尔的传说"
loreList["Clinician Korbek's experimental notes part one"] =  "巫医库贝克的实验报告，第一部分"
loreList["Clinician Korbek's experimental notes part two"] =  "巫医库贝克的实验报告，第二部分"
loreList["Clinician Korbek's experimental notes part three"] =  "巫医库贝克的实验报告，第三部分"
loreList["Clinician Korbek's experimental notes part four"] =  "巫医库贝克的实验报告，第四部分"
loreList["Captain Gumlarat's report"] =  "纲勒瑞特队长的报告"
loreList["Ukruk's log"] = "乌克鲁克的日志"

--rhaloren
loreList["letter (rhaloren camp)"] =  "信（罗兰精灵营地）"
loreList["carefully preserved letter (rhaloren camp)"] =  "小心保存的信（罗兰精灵营地）"

--sandworm
loreList["song of the sands"] =  "流沙之歌"

--scintillating-caves
loreList["research journal part 1"] =  "研修旅行笔记1"
loreList["research journal part 2"] =  "研修旅行笔记2"
loreList["research journal part 3"] =  "研修旅行笔记3"
loreList["research journal part 4"] =  "研修旅行笔记4"
loreList["research journal part 5"] =  "研修旅行笔记5"
loreList["exploration journal"] =  "探险笔记"

--shertul
loreList["first mural painting"] =  "第一幅壁画"
loreList["second mural painting"] =  "第二幅壁画"
loreList["third mural painting"] =  "第三幅壁画"
loreList["fourth mural painting"] =  "第四幅壁画"
loreList["fifth mural painting"] =  "第五幅壁画"
loreList["sixth mural painting"] =  "第六幅壁画"
loreList["seventh mural painting"] =  "第七幅壁画"
loreList["eighth mural painting"] =  "第八幅壁画"
loreList["ninth mural painting"] =  "第九幅壁画"
loreList["Yiilkgur raising toward the sky"] =  "伊克格，飞向天空"
loreList["a living Sher'Tul?!"] =  "活着的夏·图尔人?!"
loreList["lost farportal"] =  "失落的传送门"

--slazish
loreList["conch (1)"] =  "贝壳(1)"
loreList["conch (2)"] =  "贝壳(2)"
loreList["conch (3)"] =  "贝壳(3)"

--spellblaze
loreList["draft letter (mark of the Spellblaze)"] =  "草稿（魔法大爆炸之痕）"

--spellhunt
loreList["memories of archmage Varil"] =  "大法师沃利尔的回忆录"
loreList["Spellhunter's Guide part 1: How to Detect a Spellweaver"] =  "猎魔人指南第一部分：怎样辨别法师"
loreList["Spellhunter's Guide part 2: How to Battle a Magic-User"] =  "猎魔人指南第二部分：怎样与法师战斗"
loreList["Spellhunter's Guide part 3: How to Kill a Magic-User"] =  "猎魔人指南第三部分：如何杀死法师"

--sunwall
loreList["Loremaster Verutir's note"] =  "博学者温罗提的报告"

--tannen

--trollmire
loreList["tattered paper scrap (trollmire)"] =  "破烂的碎纸片（巨魔沼泽）"

--zigur
loreList["Rules of the Ziguranth"] =  "伊格兰斯守则"
loreList["The Great Evil"] =  "罪恶之源"
loreList["The story of my salvation"] =  "我的救赎"

function loreListCHN(name)
	if not name then return end
	if loreList[name] then name = loreList[name]
	else
		local key = name
		if objCHN[key] then
			name = name:gsub(objCHN[key].enName,objCHN[key].chName)
		end
	end
	return name
end

--DLC lores
loreCHN = {}
dofile("data-chn123/lore/statue.lua")
loreCat["Ashes of Urh'Rok"] = "乌鲁克之灰烬"
loreList["Lost Memories (1)"] = "遗失的记忆（1）"
loreList["Lost Memories (2)"] = "遗失的记忆（2）"



loreCHN["遗失的记忆（1）"] = [[
#{italic}#你紧紧地抱住自己的头，一踏入下一层，被囚禁的记忆如潮水般涌来……#{normal}#

"很 好 ， 下 一 个 。 " 红 宝 石 色 皮 肤 的 指 挥 道 。 一 小 群 矮 小 的 、 黑 绿 的 东 西 擦 干 了 曾 是 矮 人 的 灰 烬 ， 你 走 到 队 首 ， 站 在 矮 人 刚 才 的 位 置 。 在 房 间 的 另 一 边 ， 另 一 个 同 族 站 在 同 样 的 平 台 上 。 这 让 你 有 些 激 动 — — 你 被 选 中 为 实 验 对 象 ！ 你 希 望 你 能 对 他 们 的 研 究 有 所 助 益 ， 希 望 能 找 到 更 加 有 效 、 更 加 “ 痛 苦 ” 的 方 法 ， 同 时 期 望 着 痛 苦 的 降 临 。 一 只 绿 色 的 小 恶 魔 — — 形 态 似 乎 有 些 变 异 ， 比 你 通 常 见 到 的 骨 节 更 加 突 出 — — 站 在 你 身 边 ， 充 满 期 待 地 微 笑 着 。 另 一 边 ， 一 只 较 为 正 常 的 小 恶 魔 走 向 你 的 伙 伴 。 “ 控 制 组 准 备 … … 开 始 ” 那 只 正 常 的 酸 液 树 魔 踩 在 他 脸 上 ， 紧 紧 地 按 住， 同 时 一 股 酸 液 从 皮 肤 中 涌 出 。 红 色 的 小 恶 魔 看 了 看 他 融 化 的 肉 体 ， 略 记 了 些 什 么 ， 然 后 小 声 说 道 “ 痛 苦 阻 断 关 闭 … … ” 他 脸 上 的 笑 容 突 然 消 失 了 ， 他 抓 着 自 己 的 脸 ， 然 后 发 出 痛 苦 地 哀 嚎 ， 倒 在 地 上 ， 不 停 翻 滚 着 。 红 色 小 恶 魔 再 次 低 下 头 看 了 看 ， 厌 倦 地 叹 了 口 气 ， 继 续 写 着 。 “ 好 吧 ， 痛 苦 阻 断 打 开 ， 把 他 带 走 。 ” 他 停 止 了 翻 滚 ， 站 了 起 来 ， 再 度 微 笑 ， 酸 液 继 续 吞 噬 着 他 的 血 肉 , 直 到 房 间 里 的 管 子 朝 他 喷 洒 了 药 剂。 另 一 只 红 色 小 恶 魔 咕 哝 了 几 句 ， 摆 摆 手 ， 带 着 他 离 开 了  。 同 时 ， 你 看 见 他 身 上 可 怕 的 伤 害 渐 渐 愈 合。

“实 验 组 … … 开 始 ” 你 还 没 反 应 过 来 ， 那 只 变 异 的 酸 液 树 魔 已 经 踩 在 你 脸 上 了 。 当 酸 液 滴 落 在 你 躯 干 上 时 ， 你 并 没 有 感 到 疼 痛 ， 只 有 一 种 古 怪 的 皮 肤 消 融 的 感 觉 。 小 恶 魔 再 次 低 头 向 下 看 ； 显 然 他 漏 看 了 你 的 眼 睛 ， 因 为 你 能 看 到 他 神 色 中 的 失 望 。 “结 构 伤 害 … … 好 的 ， 痛 苦 阻 断 解 除 ” 你 认 为 下 一 步 骤 里 大 家 都 期 望 着 你 不 会 发 出 尖 叫。

你 尖 叫 着 ， 痛 苦 地 尖 叫 着 ， 发 出 你 一 生 从 未 有 过 的 哀 嚎 ， 你 甚 至 以 为 自 己 的 声 带 将 被 撕 裂 。 这 痛 苦 比 以 往 的 任 何 痛 苦 更 加 强 烈 ， 比 任 何 感 觉 都 要 强 烈 。你 跌 倒 在 地 ， 用 手 抓 挠 着 皮 肤 ， 希 望 将 酸 液 除 去 。 你 挣 扎 着 ， 抓 挠 着 ， 攥 住 手 边 任 何 东 西 ， 希 望 能 解 除 痛 苦 ， 尽 管 你 心 里 清 楚 这 并 不 可 能。

“很 好 ， 非 常 好 ！ 痛 苦 阻 断 打 开 ！ ” 刚 听 到 这 里 ， 刚 才 的 痛 苦 顿 时 消 失 了； 你 的 皮 肤 仍 在 颤 抖 ，但 痛 苦 不 再 ， 甚 至 喉 咙 也 不 疼 了 。 “ 洗 干 净 ， 带 下 去。 痛 苦 加 倍 … … 重 要 改 进 … … 建 议 同 低 痛 高 腐 蚀 型 杂 交 。 ” 一 股 不 知 名 的 液 体 朝 你 喷 射 ， 你 的 皮 肤 不 再 溶 解 。 你 的 “ 主 人 ” 抓 住 你 的 胳 膊 ， 带 着 你 离 开 ， 同 时 向 你 释 放 治 疗 法 术 。 你 的 伤 痕 消 褪 了 。你 希 望 他 们 不 会 因 为 你 的 尖 叫 而 对 你 加 以 惩 处 ， 不 过 ， 从 你 听 到 的 只 言 片语 ， 你 应 该 对 他 们 有 所 帮 助 。 想 到 这 里 ， 你 不 禁 咧 嘴 笑 了 ， 身 后 的 门 关 闭 时 ， 回 响 着 那 只 小 恶 魔 叫 喊 “ 下 一 个 ” 的 声 音 。

#{italic}#你不禁打了个寒颤。#{normal}#

]]

loreCHN["遗失的记忆（2）"] = [[
#{italic}#更多的记忆涌来#{normal}#

“记 住 ， 这 是 你 的 幸 运 日 ， ” 你 的 “ 主 人 ” 带 着 你 到 一 块 巨 大 的 闪 耀 水 晶 面 前 ， “ 你 非 常 合 作 ， 因 此 你 将 从 一 般 实 验 中 解 放。  现 在 你 被 提 升 为 研 究 助 理 ！ ” 闻 言 ， 你 欣 喜 若 狂 ！ “ 好 了 ， 现 在 我 们 需 要 先 做 一 些 事 情 。 火 焰 防 护 在 这 ， 忠 诚 强 化 在 那 ， 标 准 化 思 维 修 改 ， 呃 ，不 过 主 要 还 是 将 你 的 意 识 链 接 到  这 里 ， ” 他 一 边 说 ， 一 边 指 着 水 晶 体 , " 有 了 这 个 ， 你 看 到 的 、 听 到的 、 嗅 到 的 、 尝 到  的 、感  觉 到 的 全 都 能 体 现 在 这 块 美 丽 精 巧 的 水 晶  里 。   不  仅 如 此 ，不 管 你 去 哪 ， 不 管 你想 什 么 ， 都 能 记 录 下 来 ， 以 供研 究 。 " 你 突 然 发 现 自 己 能帮 助 他 们   研究 如 此 之 多 ！ “ 站在 那 里 别 动 ，举 起 胳 膊  ，这 样 我 就 能 把 它 放 好 ……”

那 块 水 晶 ， 对 ， 那 就 是 他 们 追 踪 你 的 方 式 ， 同 时 也 记 载了 他 们 对 你 的大 部 分 研 究 。 如 果 你 摧 毁 了 它， 你 就 能 避 开 他 们 的 注意 ， 逃 出 这 里 ， 不 被他 们 发 现 。 同 时 ，还 能 毁 了 他 们 的 部 分 研 究 成 果。 你 必 须 要 摧 毁它  ，然 后 逃 跑 ！
]]
