﻿egosCHN = {}

--ammo
-- Techniques
egosCHN["keeper's "] = "守卫者的"
egosCHN["barbed "] = "倒刺的"
egosCHN["deadly "] = "致命的"
egosCHN["high-capacity "] = "大容量的"
egosCHN["quick-loading  "] = "快速装填的"
egosCHN[" of accuracy"] = "精准之"
egosCHN[" of crippling"] = "致残之"
-- Greater
egosCHN["battle-ranger's "] = "战争先锋的"
egosCHN[" of annihilation"] = "湮灭之"
-- Arcane Egos
egosCHN["acidic "] = "强酸的"
egosCHN["arcing "] = "电弧的"
egosCHN["flaming "] = "燃烧的"
egosCHN["icy "] = "冰冷的"
egosCHN["self-loading "] = "自动装填的"
egosCHN[" of daylight"] = "黎明之"
egosCHN[" of vileness"] = "卑劣之"
egosCHN[" of paradox"] = "时空之"
-- Greater Egos
egosCHN["elemental "] = "元素的"
egosCHN["enchanted "] = "附魔的"
egosCHN["plaguebringer's "] = "瘟神的"
egosCHN["sentry's "] = "哨兵的"
egosCHN[" of corruption"] = "堕落之"
-- Nature/Antimagic Egos:
egosCHN["blazing "] = "闪耀的"
egosCHN["huntsman's "] = "猎人的"
egosCHN["insidious "] = "狡猾的"
egosCHN["storming "] = "风暴的"
egosCHN["tundral "] = "冰原的"
egosCHN[" of erosion"] = "侵蚀之"
egosCHN[" of wind"] = "风之"
-- Greater
egosCHN[" of gravity"] = "重力之"
egosCHN[" of warping"] = "扭曲之"
-- Antimagic
egosCHN["manaburning "] = "法力燃烧的"
egosCHN["slimey "] = "史莱姆的"
egosCHN[" of purging"] = "清除之"
-- Greater
egosCHN["inquisitor's "] = "审判者的"
egosCHN["slimey-burst "] = "爆裂史莱姆的"
egosCHN[" of disruption"] = "分裂之"
egosCHN[" of the leech"] = "水蛭之"
-- Psionic Egos: 
egosCHN["hateful "] = "仇恨的"
egosCHN["thought-forged "] = "思维锻造的"
egosCHN[" of psychokinesis"] = "心灵传动的"
egosCHN[" of amnesia"] = "健忘之"
-- Greater
egosCHN[" of torment"] = "折磨之"
egosCHN["pouch of "] = "一袋"
egosCHN["quiver of "] = "一袋"


--amulets
-- Stat boosting amulets
egosCHN[" of cunning "] = "灵巧之"
egosCHN[" of willpower "] = "意志之"
egosCHN[" of dexterity "] = "敏捷之"
egosCHN[" of strength "] = "力量之"
egosCHN[" of constitution "] = "体质之"
egosCHN[" of magic "] = "魔法之"
egosCHN[" of mastery "] = "精通之"
-- Immunity/Resist amulets
egosCHN["insulating "] = "隔热的"
egosCHN["grounding "] = "绝缘的"
egosCHN["anchoring "] = "稳固的"
egosCHN["inertial "] = "惯性的"
egosCHN["clarifying "] = "清晰的"
egosCHN["shielding "] = "防护的"
egosCHN["cleansing "] = "清洁的"
egosCHN["purifying "] = "净化的"
egosCHN[" of teleportation"] = "传送之"
egosCHN["starlit "] = "星光的"
-- The rest
egosCHN[" of the fish"] = "鱼之"
egosCHN[" of seduction"] = "诱惑之"
egosCHN["restful "] = "放松的"
egosCHN["vitalizing "] = "活力的"
egosCHN[" of murder"] = "谋杀之"
egosCHN[" of vision"] = "视觉之"
egosCHN[" of healing"] = "治疗之"
egosCHN["protective "] = "保护的"
egosCHN["enraging "] = "暴怒的"
egosCHN["archmage's "] = "大法师的"
egosCHN["warmaker's "] = "好战者的"
egosCHN["mindweaver's "] = "心灵编织者的"
egosCHN["savior's "] = "救世主的"
egosCHN["wanderer's "] = "流浪者的"
egosCHN["serendipitous "] = "侥幸的"
egosCHN[" of soulsearing"] = "灼热灵魂之"
egosCHN[" of manastreaming"] = "法力汹涌之"
egosCHN[" of the chosen"] = "天选之"


--armor
-- Common Resists
egosCHN[" of fire resistance"] = "火焰抵抗之"
egosCHN[" of cold resistance"] = "冰冷抵抗之"
egosCHN[" of acid resistance"] = "酸性抵抗之"
egosCHN[" of lightning resistance"] = "闪电抵抗之"
egosCHN[" of temporal resistance"] = "时空抵抗之"
egosCHN["prismatic "] = "光彩夺目的"
egosCHN["cleansing "] = "清洁的"
-- Saving Throws
egosCHN[" of stability"] = "稳定之"
egosCHN[" of spell shielding"] = "法术护盾之"
egosCHN[" of clarity"] = "清晰之"
-- Others
egosCHN["spiked "] = "尖锐的"
egosCHN["searing "] = "灼热的"
egosCHN["radiant "] = "灿烂的"
egosCHN[" of Eyal"] = "埃亚尔之"
egosCHN["enlightening "] = "启迪的"
egosCHN[" of command"] = "命令之"
egosCHN[" of delving"] = "探测之"
egosCHN[" of the deep"] = "深渊之"
egosCHN[" of thunder"] = "闪电之"	
egosCHN["rejuvenating "] = "回复的"
egosCHN["of resilience "] = "恢复之"


--belt
egosCHN[" of carrying"] = "携带之"
egosCHN["cleansing "] = "清洁的"
egosCHN["insulating "] = "隔热的"
egosCHN["grounding "] = "绝缘的"
egosCHN["nightruned "] = "暗夜符文的"
egosCHN[" of dampening"] = "抑制之"
egosCHN["blurring "] = "模糊的"
egosCHN["reinforced "] = "强化的"
egosCHN[" of transcendence"] = "卓越之"
egosCHN[" of the mystic"] = "神秘之"
egosCHN[" of the giants"] = "泰坦之"
egosCHN["monstrous "] = "怪物的"
egosCHN["balancing "] = "平衡的"
egosCHN[" of recklessness"] = "鲁莽之"
egosCHN[" of burglary"] = "盗贼之"
egosCHN["ravager's "] = "破坏者的"
egosCHN["skylord's "] = "天空领主的"
egosCHN["spiritwalker's "] = "灵魂行者的"
egosCHN[" of magery"] = "魔法之"
egosCHN[" of unlife"] = "死亡之"
egosCHN[" of the vagrant"] = "漂泊之"	
egosCHN[" of shielding"] = "防护之"
egosCHN[" of life"] = "生命之"
egosCHN[" of resilience"] = "恢复之"
egosCHN[" of valiance"] = "英勇之"
egosCHN[" of containment"] = "约束之"


--boots
egosCHN[" of tirelessness"] = "坚韧之"
egosCHN["traveler's "] = "旅行者的"
egosCHN["scholar's "] = "学者的"
egosCHN["miner's "] = "矿工的"
egosCHN[" of phasing"] = "相位之"
egosCHN[" of uncanny dodging"] = "鬼魅身法之"
egosCHN[" of speed"] = "速度之"
egosCHN[" of rushing"] = "冲锋之"
egosCHN[" of void walking"] = "虚空行走之"
egosCHN[" of disengagement"] = "脱离之"
egosCHN["blood-soaked "] = "浴血的"
egosCHN["restorative "] = "振奋的"
egosCHN["invigorating "] = "精力充沛的"
egosCHN["blightbringer's "] = "瘟神的"
egosCHN["wanderer's "] = "流浪者的"
egosCHN["reinforced "] = "强化的"
egosCHN["eldritch "] = "可怕的"
egosCHN[" of massiveness"] = "巨型的"
egosCHN[" of invasion"] = "侵犯之"
egosCHN[" of spellbinding"] = "魔法咒印之"
egosCHN[" of evasion"] = "回避之"
egosCHN["insulating "] = "隔热的"
egosCHN["grounding "] = "绝缘的"
egosCHN["dreamer's "] = "梦想家的"
egosCHN[" of strife"] = "冲突之"


--bow
egosCHN[" of dexterity "] = "敏捷之"
egosCHN["thaloren "] = "自然精灵之"


--charged-attack
egosCHN["charged "] = "充能的"

--charged-defensive
egosCHN["charged "] = "充能的"

--charged-utility
egosCHN["charged "] = "充能的"


--charms
egosCHN["quick "] = "快速的"
egosCHN["supercharged "] = "超压的"
egosCHN["overpowered "] = "压制的"


--cloak
egosCHN["shadow "] = "阴影的"
egosCHN["thick "] = "厚的"
egosCHN[" of Eldoral"] = "艾多拉之"
egosCHN[" of the Shaloren"] = "永恒精灵之"
egosCHN[" of Iron Throne"] = "钢铁王座之"
egosCHN[" of fog"] = "雾之"
egosCHN[" of protection"] = "保护之"
egosCHN[" of implacability"] = "无情之"
egosCHN["resilient "] = "恢复之"
egosCHN["enveloping "] = "遮盖的"
egosCHN["lightening "] = "减轻的"
egosCHN["regal "] = "庄严的"
egosCHN["restorative "] = "振奋的"
egosCHN["wyrmwaxed "] = "龙涎的"
egosCHN["battlemaster's "] = "战斗大师的"
egosCHN[" of sorcery"] = "幻术之"
egosCHN[" of mindcraft"] = "心灵锻造之"
egosCHN["spellcowled "] = "暗影蒙面的"
egosCHN["marshal's "] = "元帅的"
egosCHN["murderer's "] = "谋杀者的"
egosCHN[" of the guardian"] = "保护者之"
egosCHN[" of conjuring"] = "魔术之"
egosCHN[" of battle"] = "好战的"
egosCHN[" of the hunt"] = "狩猎之"
egosCHN[" of backstabbing"] = "暗算之"	


--digger
egosCHN[" of delving"] = "探测之"
egosCHN[" of endurance"] = "耐久之"
egosCHN["miner's "] = "矿工的"
egosCHN["woodsman's "] = "樵夫的"
egosCHN[" of the Iron Throne"] = "钢铁王座之"
egosCHN[" of Reknor"] = "瑞库纳之"
egosCHN["brutal "] = "野蛮的"
egosCHN["dream-smith's "] = "梦境锻造的"
egosCHN["soldier's "] = "士兵的"
egosCHN["bloodhexed "] = "嗜血的"
egosCHN["crystalomancer's "] = "水晶术士的"
egosCHN["shattering "] = "破碎的"
egosCHN["sapper's "] = "工兵的"
egosCHN["dwarven "] = "矮人的"
egosCHN[" of quickening"] = "加速之"
egosCHN[" of predation"] = "掠夺之"
egosCHN[" of deeplife"] = "深渊生命之"
egosCHN[" of wreckage"] = "残骸之"	


--gloves
egosCHN["cinder "] = "灰烬的"
egosCHN["corrosive "] = "腐蚀的"
egosCHN["naturalist's "] = "自然主义者的"
egosCHN["polar "] = "两极的"
egosCHN["psychic's "] = "灵魂的"
egosCHN["sand "] = "沙质的"
egosCHN["storm "] = "暴风的"
-- arcane/pure single target talent procs/higher chance
egosCHN["blighted "] = "枯萎的"
egosCHN["umbral "] = "黑暗的"
egosCHN["radiant "] = "光明的"
egosCHN["temporal "] = "时空的"
-- stats
egosCHN[" of dexterity "] = "敏捷之"
egosCHN[" of magic "] = "魔法之"
egosCHN[" of strength "] = "力量之"
-- the rest
egosCHN["restful "] = "放松的"
egosCHN["steady "] = "稳固的"
egosCHN[" of war-making"] = "战争制造之"
egosCHN[" of the iron hand"] = "铁腕之"
egosCHN["alchemist's "] = "炼金术师的"
egosCHN["brawler's "] = "格斗家的"
egosCHN[" of butchering"] = "屠杀之"
egosCHN[" of the nighthunter"] = "暗夜猎手之"
egosCHN["stone warden's "] = "石头守卫的"
egosCHN[" of the starseeker"] = "寻星者之"
egosCHN[" of dispersion"] = "驱散之"
egosCHN[" of regeneration"] = "回复之"
egosCHN[" of sorrow"] = "悲伤之"
egosCHN[" of the juggernaut"] = "主宰之"
egosCHN["spellstreaming "] = "法术汹涌的"
egosCHN["heroic "] = "英雄的"
egosCHN["archer's "] = "射手的"


--heavy-armor
egosCHN["impenetrable "] = "执着的"
egosCHN["hardened "] = "硬化的"
egosCHN["fearforged "] = "恐惧降临的"
egosCHN[" of implacability"] = "无情之"
egosCHN["fortifying "] = "加固的"


--helm
egosCHN[" of strength "] = "力量之"
egosCHN[" of constitution "] = "体质之"
egosCHN[" of dexterity "] = "敏捷之"
egosCHN["thaloren "] = "自然精灵的"
egosCHN["prismatic "] = "光彩夺目的"
egosCHN[" of precognition"] = "预言之"
egosCHN[" of the depths"] = "深渊之"
egosCHN[" of absorption"] = "吸收之"
egosCHN["miner's "] = "矿工的"
egosCHN["insulating "] = "隔热的"
egosCHN["grounding "] = "绝缘的"
egosCHN["stabilizing "] = "稳定的"
egosCHN["cleansing "] = "清洁的"
egosCHN[" of knowledge"] = "学识之"
egosCHN[" of might"] = "威力之"
egosCHN[" of trickery"] = "欺诈之"
egosCHN["warlord's "] = "战争领主的"
egosCHN["defender's "] = "保护者的"
egosCHN["dragonslayer's "] = "弑龙者的"
egosCHN["werebeast's "] = "兽人的"
egosCHN["mindcaging "] = "精神牢笼的"
egosCHN["champion's "] = "冠军的"
egosCHN["leafwalker's "] = "绿叶行者的"
egosCHN["catburglar's "] = "飞贼的"
egosCHN[" of blood magic"] = "血魔法之"
egosCHN[" of fortune"] = "幸运之"
egosCHN[" of sanctity"] = "圣洁之"
egosCHN[" of ire"] = "忿怒之"
egosCHN["bladed "] = "刀刃的"
egosCHN[" of the bounder"] = "莽汉之"

	
--infusions
egosCHN[" of the warrior"] = "战士的"
egosCHN[" of the duelist"] = "角斗士的"
egosCHN[" of the wizard"] = "法师的"
egosCHN[" of the psychic"] = "心灵的"
egosCHN[" of the sneak"] = "潜行的"
egosCHN[" of the titan"] = "泰坦的"	


--light-armor
egosCHN["troll-hide "] = "巨魔的"
egosCHN["nimble "] = "灵敏的"
egosCHN["marauder's "] = "掠夺者的"
egosCHN[" of the sky"] = "天空之"
egosCHN[" of Toknor"] = "图库纳之"
egosCHN[" of the wind"] = "轻风之"
egosCHN["multi-hued "] = "多彩的"
egosCHN["caller's "] = "呼唤者的"
egosCHN["miasmic "] = "毒气的"
egosCHN["aetheric "] = "以太的"
egosCHN[" of the void"] = "虚空之"
--light-boots
egosCHN["stealthy "] = "潜行的"


--lite
egosCHN["bright "] = "明亮的"
egosCHN[" of the sun"] = "太阳之"
egosCHN[" of the moons"] = "月亮之"
egosCHN["scorching "] = "灼热的"
egosCHN[" of the forge"] = "锻造之"
egosCHN["burglar's "] = "窃贼的"
egosCHN[" of clarity"] = "清晰之"
egosCHN[" of health"] = "健康之"
egosCHN["survivor's "] = "幸存者的"
egosCHN["preserving "] = "保持的"
egosCHN["piercing "] = "穿透的"
egosCHN["dreamer's "] = "梦想家的"
egosCHN["void-walker's "] = "虚空行者的"
egosCHN["nightwalker's "] = "暗夜行者的"
egosCHN[" of illusion"] = "幻觉之"
egosCHN[" of corpselight"] = "鬼火之"
egosCHN[" of the zealot"] = "狂热之"
egosCHN[" of focus"] = "聚焦之"
egosCHN["ethereal "] = "缥渺的"
egosCHN["watchleader's "] = "守望领主的"


--massive-armor
egosCHN[" of the dragon"] = "龙之"
egosCHN["impenetrable "] = "执着的"	
egosCHN["hardened "] = "硬化的"
egosCHN["fearforged "] = "恐惧降临的"
egosCHN[" of implacability"] = "无情之"
egosCHN["fortifying "] = "加固的"


--mindstars
--Nature and Antimagic
egosCHN["blooming "] = "绽放的"
egosCHN["gifted "] = "天才的"
egosCHN["nature's "] = "自然的"
egosCHN[" of balance"] = "平衡之"
egosCHN[" of life"] = "生命之"
egosCHN[" of slime"] = "史莱姆之"
egosCHN[" of the jelly"] = "果冻之"
--Psionic
egosCHN["horrifying "] = "恐惧的"
egosCHN["radiant "] = "灿烂的"
egosCHN[" of clarity"] = "清晰之"
egosCHN["hungering "] = "饥渴的"
egosCHN[" of nightfall"] = "黄昏之"
egosCHN["creative "] = "创造力的"
egosCHN[" of resolve"] = "分解之"
-- Mindstar Sets 
egosCHN["harmonious "] = "和谐的"
egosCHN["resonating "] = "共鸣的"
egosCHN["honing "] = "研磨的"
egosCHN["parasitic "] = "寄生的"
-- Caller's Set: For summoners!
egosCHN["caller's "] = "呼唤者的"
egosCHN["summoner's "] = "召唤师的"
-- Drake sets
egosCHN["wyrm's "] = "龙战士的"
egosCHN[" of flames"] = "火焰之"
egosCHN[" of frost"] = "冰冻之"
egosCHN[" of sand"] = "沙之"
egosCHN[" of storms"] = "风暴之"
egosCHN[" of venom"] = "毒液之"
-- Mentalist Set
egosCHN["dreamer's "] = "梦想家的"
egosCHN["epiphanous "] = "光辉的"

egosCHN["absorbing "] = "吸能的"
egosCHN["projecting "] = "辐射的"
-- Mitotic Set
egosCHN["mitotic "] = "分裂的"
-- Wrathful Set
egosCHN["hateful "] = "可恶的"
egosCHN["wrathful "] = "愤怒的"


--potions
egosCHN["acid-proof "] = "耐酸的 "
egosCHN["giant "] = "巨大的 "

--ranged
-- Techniques
egosCHN["mighty "] = "强大的"
egosCHN["ranger's "] = "射手的"
egosCHN["steady "] = "稳固的"
egosCHN[" of power"] = "能量之"
-- Greater
egosCHN["swiftstrike "] = "飞燕的"
egosCHN[" of true flight"] = "飞驰之"
-- Arcane Egos
egosCHN[" of fire"] = "火焰之"
egosCHN[" of cold"] = "冰冷之"
egosCHN[" of acid"] = "强酸之"
egosCHN[" of lightning"] = "闪电之"
-- Greater
egosCHN["penetrating "] = "穿透的"
egosCHN["runic "] = "符文的"
egosCHN["warden's "] = "守卫者的"
egosCHN[" of recursion"] = "循环之"
-- Nature/Antimagic Egos
egosCHN["fungal "] = "真菌的"
-- Greater
egosCHN["blazebringer's "] = "烈焰行者的"
egosCHN["caustic "] = "腐蚀的"
egosCHN["glacial "] = "极寒的"
egosCHN["thunderous "] = "雷电的"
egosCHN[" of nature"] = "自然之"
-- Antimagic
egosCHN[" of dampening"] = "抑制之"
egosCHN["mage-hunter's "] = "猎法者的"
egosCHN["throat-seeking "] = "伤喉的"
-- Psionic Egos
egosCHN["psychic's "] = "灵魂的"


--rings
-- Immunity/Resist rings
egosCHN[" of sensing"] = "感觉之"
egosCHN[" of clarity"] = "清晰之"
egosCHN[" of tenacity"] = "固执之"
egosCHN[" of perseverance"] = "不懈之"
egosCHN[" of arcana"] = "奥秘之"
egosCHN[" of fire "] = "火焰之"
egosCHN[" of frost "] = "冰冻之"
egosCHN[" of nature "] = "自然之"
egosCHN[" of lightning "] = "闪电之"
egosCHN[" of light "] = "光之"
egosCHN[" of darkness "] = "暗之"
egosCHN[" of corrosion "] = "腐蚀之"
-- rare resists
egosCHN[" of aether "] = "以太抵抗之"
egosCHN[" of blight "] = "枯萎抵抗之"
egosCHN[" of the mountain "] = "精神抵抗之"
egosCHN[" of the mind "] = "物理抵抗之"
egosCHN[" of the time "] = "时空抵抗之"
-- The rest
egosCHN[" of power "] = "能量之"
egosCHN["savior's "] = "救世主的"
egosCHN["warrior's "] = "战士的"
egosCHN["rogue's "] = "盗贼的"
egosCHN["marksman's "] = "神射手的"
egosCHN["titan's "] = "泰坦的"	
egosCHN["wizard's "] = "法师的"
egosCHN["psionicist's "] = "灵能者的"
egosCHN["sneakthief's "] = "刺客的"
egosCHN["gladiator's "] = "角斗者的"
egosCHN["conjurer's "] = "魔术师的"
egosCHN["solipsist's "] = "织梦者的"
egosCHN["mule's "] = "执着者的"
egosCHN[" of life"] = "生命之"
egosCHN["painweaver's "] = "痛苦编织者的"
egosCHN["savage's "] = "野蛮人的"
egosCHN["treant's "] = "树精的"
egosCHN[" of misery"] = "痛苦之"
egosCHN[" of warding"] = "守护之"
egosCHN[" of focus"] = "专注之"
egosCHN[" of pilfering"] = "偷窃之"
egosCHN[" of speed"] = "速度之"
egosCHN[" of blinding strikes"] = "炫目之"	


--robe
-- Resists and saves
egosCHN[" of fire "] = "火焰抵抗之"
egosCHN[" of frost "] = "冰冷抵抗之"
egosCHN[" of nature "] = "自然抵抗之"
egosCHN[" of lightning "] = "闪电抵抗之"
egosCHN[" of light "] = "光系抵抗之"
egosCHN[" of darkness "] = "暗影抵抗之"
egosCHN[" of corrosion "] = "腐蚀抵抗之"
-- rare resists
egosCHN[" of blight "] = "枯萎抵抗之"
egosCHN[" of the mountain "] = "泰山抵抗之"
egosCHN[" of the mind "] = "精神抵抗之"
egosCHN[" of time "] = "时间抵抗之"
-- Arcane Damage doesn't get resist too so we give it +mana instead
egosCHN["shimmering "] = "闪烁的"
-- Saving Throws (robes give good saves)
egosCHN[" of protection"] = "保护之"
egosCHN["dreamer's "] = "梦想家的"
egosCHN["dispeller's "] = "消融者的"
-- The rest
egosCHN[" of the elements"] = "元素之"
egosCHN["spellwoven "] = "法力编织的"
egosCHN[" of Linaniil"] = "莱娜尼尔之"
egosCHN[" of Angolwen"] = "安格利文之"
egosCHN["stargazer's "] = "占星师的"
egosCHN["ancient "] = "远古的"
egosCHN[" of power"] = "能量之"
egosCHN[" of chaos"] = "混沌之"
egosCHN["sunsealed "] = "日食的"
egosCHN[" of life"] = "生命之"
egosCHN["slimy "] = "史莱姆的"
egosCHN["stormlord's "] = "风暴领主的"
egosCHN["verdant "] = "翠绿的"
egosCHN["mindwoven "] = "心灵编织的"
egosCHN["tormentor's "] = "折磨者的"	
egosCHN["focusing "] = "集中的"	
egosCHN["fearwoven "] = "恐惧编织的"
egosCHN["psion's "] = "心灵术士的"


--scrolls
egosCHN["fire-proof "] = "耐火的 "
egosCHN["long "] = "很长的"

--shield
-- resists... shields are very good at providing resists
egosCHN[" of fire resistance "] = "火焰抵抗之"
egosCHN[" of cold resistance "] = "冰冷抵抗之"
egosCHN[" of acid resistance "] = "酸性抵抗之"
egosCHN[" of lightning resistance "] = "闪电抵抗之"
-- rare resists
egosCHN[" of arcane resistance "] = "奥术抵抗之"
egosCHN[" of mind resistance "] = "精神抵抗之"
egosCHN[" of physical resistance "] = "物理抵抗之"
egosCHN[" of purity"] = "纯洁之"
egosCHN[" of reflection"] = "反射之"
egosCHN[" of temporal resistance "] = "时空抵抗之"
egosCHN[" of resistance"] = "抵抗之"
-- retalation/melee_project shields
egosCHN["flaming "] = "燃烧的"
egosCHN["icy "] = "冰冷的"
egosCHN["shocking "] = "电击的"
egosCHN["acidic "] = "强酸的"
egosCHN[" of gloom"] = "阴暗之"
egosCHN["coruscating "] = "闪光的"
egosCHN["crackling "] = "爆裂的"
egosCHN["corrosive "] = "腐蚀的"
egosCHN["wintry "] = "寒冷的"
egosCHN["living "] = "生存的"
egosCHN[" of the forge"] = "锻造之"
egosCHN[" of patience"] = "忍耐之"
egosCHN[" of the sun"] = "太阳之"
-- The rest
egosCHN["reinforced "] = "强化的"
egosCHN["impervious "] = "密实的"
egosCHN["spellplated "] = "法术刻印的"
egosCHN["blood-etched "] = "鲜血侵蚀的"
egosCHN[" of crushing"] = "征服之"
egosCHN[" of resilience"] = "恢复之"
egosCHN[" of deflection"] = "偏移之"
egosCHN[" of displacement"] = "置换之"
egosCHN[" of the earth"] = "大地之"
egosCHN[" of harmony"] = "和谐之"
egosCHN[" of faith"] = "信念之"	


--sling
egosCHN[" of cunning "] = "灵巧之"
egosCHN["halfling "] = "半身人的"

--staves
egosCHN["cruel "] = "残忍的"
egosCHN["earthen "] = "大地的"
egosCHN["potent "] = "有效的"
egosCHN["shimmering "] = "闪烁的"
egosCHN["surging "] = "冲击的"
egosCHN["blighted "] = "枯萎的"
egosCHN["ethereal "] = "飘渺的"
egosCHN["greater "] = "高级的"
egosCHN["void walker's "] = "虚空行者的"
egosCHN[" of fate"] = "命运之"
egosCHN[" of illumination"] = "照明之"
egosCHN[" of might"] = "强化之"
egosCHN[" of power"] = "能量之"
egosCHN[" of projection"] = "投射之"
egosCHN[" of warding"] = "守护之"
egosCHN[" of breaching"] = "导管之"
egosCHN[" of blasting"] = "爆炸之"
egosCHN[" of channeling"] = "传导之"
egosCHN[" of greater warding"] = "超级守护之"
egosCHN[" of invocation"] = "祈祷之"
egosCHN[" of protection"] = "保护之"
egosCHN[" of wizardry"] = "巫术之"
egosCHN["lifebinding "] = "生命赞歌的"
egosCHN["infernal "] = "地狱的"
egosCHN["bloodlich's "] = "血巫的"
egosCHN["magelord's "] = "法师领主的"
egosCHN["short "] = "短的"
egosCHN["magewarrior's short "] = "魔战士的"

--torques
egosCHN["psionic "] = "超能的"
egosCHN["warded "] = "守护的"
egosCHN["quiet "] = "宁静的"
egosCHN["telekinetic "] = "念力的"

--torques-powers
egosCHN[" of psychoportation"] = "心灵传送之"
egosCHN[" of kinetic psionic shield"] = "动能护盾之"
egosCHN[" of thermal psionic shield"] = "热能护盾之"
egosCHN[" of charged psionic shield"] = "充能护盾之"
egosCHN[" of clear mind"] = "清晰思维之"
egosCHN[" of mindblast"] = "心灵爆炸之"

--totems
egosCHN["natural "] = "自然的"
egosCHN["forceful "] = "强力的"
egosCHN["warded "] = "守护的"
egosCHN["rushing "] = "冲锋的"
egosCHN["webbed "] = "蛛网的"
egosCHN["tentacled "] = "触手的"

--totems-powers
egosCHN[" of cure illness"] = "治疗疾病之"
egosCHN[" of cure poisons"] = "净化毒素之"
egosCHN[" of thorny skin"] = "荆棘皮肤之"
egosCHN[" of healing"] = "治愈之"

--wands
egosCHN["arcane "] = "奥术的"
egosCHN["defiled "] = "肮脏的"
egosCHN["bright "] = "明亮的"
egosCHN["shadowy "] = "阴影的"
egosCHN["warded "] = "守护的"
egosCHN["void "] = "虚空的"
egosCHN["volcanic "] = "火山的"
egosCHN["striking "] = "打击的"

--wands-powers
egosCHN[" of detection"] = "侦查之"
egosCHN[" of illumination"] = "照明之"
egosCHN[" of trap destruction"] = "陷阱拆除之"
egosCHN[" of lightning"] = "闪电之"
egosCHN[" of firewall"] = "火墙之"
egosCHN[" of conjuration"] = "咒语之"


--weapon
-- Techniques
egosCHN["balanced "] = "平衡的"
egosCHN[" of crippling"] = "残废之"
egosCHN[" of massacre"] = "屠杀之"
-- Greater
egosCHN["quick "] = "快速的"
egosCHN["warbringer's "] = "战争制造者的"
egosCHN[" of evisceration"] = "开膛之"
egosCHN[" of rage"] = "愤怒之"
egosCHN[" of ruin"] = "毁灭之"
egosCHN[" of shearing"] = "修剪之"
-- Arcane Egos
egosCHN["acidic "] = "酸性的"
egosCHN["arcing "] = "电弧的"
egosCHN["flaming "] = "燃烧的"
egosCHN["icy "] = "冰冻的"
egosCHN[" of daylight"] = "黎明之"
egosCHN[" of phasing"] = "相位之"
egosCHN[" of vileness"] = "卑劣之"
egosCHN[" of paradox"] = "时空之"
-- Greater Egos
egosCHN["elemental "] = "元素的"
egosCHN["plaguebringer's "] = "瘟神的"
egosCHN[" of corruption"] = "堕落之"
egosCHN[" of the mystic"] = "神秘之"
-- Nature/Antimagic Egos:
egosCHN["huntsman's "] = "猎人之"
egosCHN["insidious "] = "狡猾的"
egosCHN[" of erosion"] = "侵蚀之"
egosCHN["blazebringer's "] = "烈焰行者的"
egosCHN["caustic "] = "腐蚀的"
egosCHN["glacial "] = "极寒的"
egosCHN["thunderous "] = "雷电的"
egosCHN[" of gravity"] = "重力之"
egosCHN[" of nature"] = "自然之"
-- Antimagic
egosCHN["manaburning "] = "法力燃烧之"
egosCHN["slime-covered "] = "强化史莱姆之"
egosCHN[" of banishment"] = "放逐之"
egosCHN[" of purging"] = "清除之"
egosCHN[" of disruption"] = "分裂之"
egosCHN[" of the leech"] = "水蛭之"
-- Psionic Egos: 
egosCHN["thought-forged "] = "思维锻造的"
egosCHN[" of amnesia"] = "健忘之"
egosCHN[" of projection"] = "投射之"
egosCHN["psychic's "] = "灵魂的"
egosCHN[" of torment"] = "折磨之"


--wizard-hat
egosCHN[" of balance "] = "平衡之"
egosCHN["augmenting "] = "扩张的"
egosCHN["eldritch "] = "可怕的"
egosCHN["cleansing "] = "清洁的"
egosCHN["insulating "] = "隔热的"
egosCHN["grounding "] = "绝缘的"
egosCHN[" of knowledge"] = "学识之"
egosCHN[" of arcana"] = "奥秘之"
egosCHN["aegis "] = "庇护的"
egosCHN[" of madness"] = "疯狂之"
egosCHN[" of the Brotherhood"] = "兄弟会之"
egosCHN[" of earthrunes"] = "大地符文之"
egosCHN["stabilizing "] = "稳定的"
egosCHN["clarifying "] = "清晰的"
egosCHN["shielding "] = "防护的"
egosCHN[" of decomposition"] = "分解之"
-- Damage and Resists
egosCHN[" of fire "] = "火焰抵抗之"
egosCHN[" of frost "] = "冰冷抵抗之"
egosCHN[" of nature "] = "自然抵抗之"
egosCHN[" of lightning "] = "闪电抵抗之"
egosCHN[" of light "] = "光系抵抗之"
egosCHN[" of darkness "] = "暗影抵抗之"
egosCHN[" of corrosion "] = "腐蚀抵抗之"
-- rare resists
egosCHN[" of blight "] = "枯萎抵抗之"
egosCHN[" of the mountain "] = "泰山抵抗之"
egosCHN[" of the mind "] = "精神抵抗之"
egosCHN[" of time "] = "时间抵抗之"
-- Arcane Damage doesn't get resist too so we give it +mana instead
egosCHN["shimmering "] = "闪烁的"
egosCHN[" of the sentry"] = "警卫之"
egosCHN["fearwoven "] = "恐惧编织的"
egosCHN["psion's "] = "心灵术士的"
egosCHN["mindwoven "] = "心灵编织的"
egosCHN["spellwoven "] = "法力编织的"


egosCHN["psychokinetic "] = "念力的"
egosCHN[" of the eclipse"] = "日食之"
egosCHN[" of perfection "] = "圆满之"
egosCHN[" of natural resilience"] = "滋养之"
egosCHN["noble's "] = "贵族之"
egosCHN["undeterred "] = "冒进的"
egosCHN[" of the hunter"] = "猎手之"
egosCHN[" of the voidstalker"] = "虚空追猎之"
egosCHN[" of the verdant"] = "苍郁之"
egosCHN["scouring "] = "洗练的"
egosCHN[" of archery"] = "弓道之"
egosCHN[" of alacrity"] = "灵动之"
egosCHN[" of the hero "] = "英雄之"
egosCHN[" of persecution"] = "迫害之"
egosCHN["pixie's "] = "小妖精的"
egosCHN[" of alchemy"] = "炼金之"
egosCHN["stormwoven "] = "织岚的"
egosCHN["gloomy "] = "阴森的"
egosCHN["deflecting "] = "偏转的"
egosCHN["scouring "] = "洗练的"
egosCHN["cosmic "] = "浩瀚的"
egosCHN[" of radiance"] = "光耀之"
egosCHN[" of earthen fury"] = "地怒之"
egosCHN["magma "] = "岩浆的"
egosCHN["sunbathed "] = "浴日的"
egosCHN[" of conveyance"] = "运输之"
egosCHN["chronomancer's "] = "御时者的"
egosCHN["abyssal "] = "深渊的"
egosCHN[" of conflagration"] = "红莲之"
egosCHN[" of ruination"] = "破灭之"
egosCHN[" of divination"] = "占卜之"
egosCHN[" of cure ailments"] = "康复之"
egosCHN[" of clairvoyance"] = "通彻之"
egosCHN["chilling "] = "寒颤的"
egosCHN[" of persecution"] = "迫害之"

--egosCHN[""] = ""
--
--
--
keyCHN = {}

keyCHN["balanced"] = "平衡"
keyCHN["massacre"] = "屠杀"
keyCHN["quick"] = "快速"
keyCHN["warbringer"] = "战争制造者"
keyCHN["crippling"] = "致残"
keyCHN["evisc"] = "开膛"
keyCHN["rage"] = "愤怒"
keyCHN["ruin"] = "毁灭"
keyCHN["shearing"] = "修剪"
keyCHN["acidic"] = "强酸"
keyCHN["arcing"] = "电弧"
keyCHN["flaming"] = "燃烧"
keyCHN["chilling"] = "寒颤"
keyCHN["daylight"] = "黎明"
keyCHN["phase"] = "相位"
keyCHN["vile"] = "卑劣"
keyCHN["paradox"] = "时空"
keyCHN["elemental"] = "元素"
keyCHN["plague"] = "瘟神"
keyCHN["corruption"] = "堕落"
keyCHN["mystic"] = "神秘"
keyCHN["insid"] = "狡猾"
keyCHN["erosion"] = "侵蚀"
keyCHN["blaze"] = "烈焰行者"
keyCHN["caustic"] = "腐蚀"
keyCHN["glacial"] = "极寒"
keyCHN["thunder"] = "雷电"
keyCHN["nature"] = "自然"
keyCHN["manaburning"] = "法力燃烧"
keyCHN["slime"] = "史莱姆"
keyCHN["banishment"] = "迫害"
keyCHN["purging"] = "清除"
keyCHN["inquisitors"] = "审判者"
keyCHN["disruption"] = "分裂"
keyCHN["leech"] = "水蛭"
keyCHN["dampening"] = "抑制"
keyCHN["projection"] = "投射"
keyCHN["thought"] = "思维锻造"
keyCHN["forgotten"] = "健忘"
keyCHN["torment"] = "折磨"
keyCHN["gravity"] = "重力"
keyCHN["barbed"] = "倒刺"
keyCHN["deadly"] = "致命"
keyCHN["capacity"] = "大容量"
keyCHN["accuracy"] = "精准"
keyCHN["crippling"] = "致残"
keyCHN["annihilation"] = "湮灭"
keyCHN["acidic"] = "强酸"
keyCHN["arcing"] = "电弧"
keyCHN["flaming"] = "燃烧"
keyCHN["icy"] = "冰冷"
keyCHN["self"] = "自动装填"
keyCHN["daylight"] = "黎明"
keyCHN["vile"] = "卑劣"
keyCHN["paradox"] = "时空"
keyCHN["elemental"] = "元素"
keyCHN["plague"] = "瘟神"
keyCHN["sentry"] = "哨兵"
keyCHN["corruption"] = "堕落"
keyCHN["fiery"] = "闪耀"
keyCHN["insid"] = "狡猾"
keyCHN["storm"] = "风暴"
keyCHN["tundral"] = "冰原"
keyCHN["erosion"] = "侵蚀"
keyCHN["wind"] = "风"
keyCHN["gravity"] = "重力"
keyCHN["manaburning"] = "法力燃烧"
keyCHN["purging"] = "清除"
keyCHN["slimeburst"] = "爆裂史莱姆"
keyCHN["disruption"] = "迫害"
keyCHN["leech"] = "水蛭"
keyCHN["hateful"] = "仇恨"
keyCHN["thought"] = "思维锻造"
keyCHN["kinesis"] = "念力"
keyCHN["amnesia"] = "健忘"
keyCHN["torment"] = "折磨"
keyCHN["mighty"] = "强大"
keyCHN["ranger"] = "战争先锋"
keyCHN["steady"] = "稳固"
keyCHN["power"] = "能量"
keyCHN["swiftstrike"] = "飞燕"
keyCHN["flight"] = "飞驰"
keyCHN["fire"] = "火焰"
keyCHN["cold"] = "冰冷"
keyCHN["acid"] = "酸性"
keyCHN["lightning"] = "闪电"
keyCHN["physical"] = "物理"
keyCHN["arcane"] = "奥术"
keyCHN["mind"] = "精神"
keyCHN["temporal"] = "时空"
keyCHN["purity"] = "纯洁"
keyCHN["reflection"] = "反射"
keyCHN["flaming"] = "火焰"
keyCHN["cold"] = "冰冷"
keyCHN["acid"] = "酸性"
keyCHN["lightning"] = "闪电"
keyCHN["physical"] = "物理"
keyCHN["arcane"] = "奥术"
keyCHN["mind"] = "精神"
keyCHN["temporal"] = "时空"
keyCHN["flaming"] = "燃烧"
keyCHN["icy"] = "冰冷"
keyCHN["shocking"] = "电击"
keyCHN["acidic"] = "强酸"
keyCHN["gloom"] = "阴暗"
keyCHN["coruscating"] = "闪光"
keyCHN["crackling"] = "爆裂"
keyCHN["corrosive"] = "腐蚀"
keyCHN["wintry"] = "寒冷"
keyCHN["living"] = "生存"
keyCHN["penetrating"] = "穿透"
keyCHN["runic"] = "符文"
keyCHN["wardens"] = "石头守卫"
keyCHN["recursion"] = "循环"
keyCHN["fungal"] = "真菌"
keyCHN["blaze"] = "烈焰行者"
keyCHN["caustic"] = "腐蚀"
keyCHN["glacial"] = "极寒"
keyCHN["thunder"] = "雷电"
keyCHN["nature"] = "自然"
keyCHN["dampening"] = "抑制"
keyCHN["magehunters"] = "猎法者"
keyCHN["throat"] = "伤喉"
keyCHN["cruel"] = "残忍"
keyCHN["earthen"] = "大地"
keyCHN["potent"] = "有效"
keyCHN["shimmering"] = "闪烁"
keyCHN["surging"] = "冲击"
keyCHN["blight"] = "枯萎"
keyCHN["ethereal"] = "飘渺"
keyCHN["greater"] = "高级"
keyCHN["['v. walkers']"] = "虚空行者"
keyCHN["fate"] = "命运"
keyCHN["illumination"] = "照明"
keyCHN["might"] = "强化"
keyCHN["power"] = "能量"
keyCHN["projection"] = "投射"
keyCHN["warding"] = "守护"
keyCHN["breaching"] = "导管"
keyCHN["blasting"] = "爆炸"
keyCHN["channeling"] = "传导"
keyCHN["['g. warding']"] = "超级守护"
keyCHN["invocation"] = "祈祷"
keyCHN["protection"] = "保护"
keyCHN["wizardry"] = "巫术"
keyCHN["lifebinding"] = "生命赞歌"
keyCHN["infernal"] = "地狱"
keyCHN["bloodlich"] = "血巫"
keyCHN["magelord"] = "法师领主"
keyCHN["short"] = "短"
keyCHN["magewarrior"] = "魔战士"
keyCHN["radiance"] = "光耀"
keyCHN["gloomy"] = "阴森"
keyCHN["deflecting"] = "偏转"
keyCHN["scouring"] = "洗练"
keyCHN["cosmic"] = "浩瀚"
keyCHN["earthen fury"] = "地怒"
keyCHN["magma"] = "岩浆"

keyCHN["blooming"] = "绽放"
keyCHN["gifted"] = "天才"
keyCHN["nature"] = "自然"
keyCHN["balance"] = "平衡"
keyCHN["life"] = "生命"
keyCHN["slime"] = "史莱姆"
keyCHN["horrifying"] = "恐惧"
keyCHN["radiant"] = "灿烂"
keyCHN["clarity"] = "清晰"
keyCHN["hungering"] = "饥渴"
keyCHN["nightfall"] = "黄昏"
keyCHN["harmonious"] = "和谐"
keyCHN["resonating"] = "共鸣"
keyCHN["callers"] = "呼唤者"
keyCHN["summoners"] = "召唤师"
keyCHN["wyrms"] = "龙战士"
keyCHN["flames"] = "火焰"
keyCHN["frost"] = "冰冻"
keyCHN["sand"] = "沙"
keyCHN["storms"] = "风暴"
keyCHN["dreamers"] = "梦想家"
keyCHN["epiphanous"] = "光辉"
keyCHN["mitotic"] = "分裂"
keyCHN["wrath"] = "愤怒"
