local Talents = require "engine.interface.ActorTalents"
dofile("data-chn123/talents/chronomancy/blade-threading.lua")
dofile("data-chn123/talents/chronomancy/bow-threading.lua")
dofile("data-chn123/talents/chronomancy/chronomancy.lua")
dofile("data-chn123/talents/chronomancy/fate-weaving.lua")
dofile("data-chn123/talents/chronomancy/energy.lua")
dofile("data-chn123/talents/chronomancy/flux.lua")
dofile("data-chn123/talents/chronomancy/gravity.lua")
dofile("data-chn123/talents/chronomancy/guardian.lua")
dofile("data-chn123/talents/chronomancy/matter.lua")
dofile("data-chn123/talents/chronomancy/other.lua")
dofile("data-chn123/talents/chronomancy/spacetime-folding.lua")
dofile("data-chn123/talents/chronomancy/spacetime-weaving.lua")
dofile("data-chn123/talents/chronomancy/spellbinding.lua")
dofile("data-chn123/talents/chronomancy/speed-control.lua")
dofile("data-chn123/talents/chronomancy/stasis.lua")
dofile("data-chn123/talents/chronomancy/temporal-combat.lua")
dofile("data-chn123/talents/chronomancy/temporal-hounds.lua")
dofile("data-chn123/talents/chronomancy/threaded-combat.lua")
dofile("data-chn123/talents/chronomancy/timeline-threading.lua")
dofile("data-chn123/talents/chronomancy/timetravel.lua")

	Talents.talents_def.T_ANOMALY_REARRANGE.name = "异常：重排"

	Talents.talents_def.T_ANOMALY_TELEPORT.name = "异常：传送"

	Talents.talents_def.T_ANOMALY_SWAP.name = "异常：换位"

	Talents.talents_def.T_ANOMALY_DISPLACEMENT_SHIELD.name = "异常：相位护盾"

	Talents.talents_def.T_ANOMALY_WORMHOLE.name = "异常：虫洞"

	Talents.talents_def.T_ANOMALY_PROBABILITY_TRAVEL.name = "异常：相位移动"

	Talents.talents_def.T_ANOMALY_BLINK.name = "异常：闪烁"

	Talents.talents_def.T_ANOMALY_SUMMON_TOWNSFOLK.name = "异常：召唤村民"

	Talents.talents_def.T_ANOMALY_SLOW.name = "异常：减速"

	Talents.talents_def.T_ANOMALY_HASTE.name = "异常：加速"

	Talents.talents_def.T_ANOMALY_STOP.name = "异常：停止"

	Talents.talents_def.T_ANOMALY_TEMPORAL_BUBBLE.name = "异常：时空气泡"

	Talents.talents_def.T_ANOMALY_TEMPORAL_SHIELD.name = "异常：时间盾"

	Talents.talents_def.T_ANOMALY_INVIGORATE.name = "异常：鼓舞"

	Talents.talents_def.T_ANOMALY_TEMPORAL_CLONE.name = "异常：克隆"

	Talents.talents_def.T_ANOMALY_TEMPORAL_STORM.name = "异常：时空风暴"

	Talents.talents_def.T_ANOMALY_GRAVITY_PULL.name = "异常：重力牵引"

	Talents.talents_def.T_ANOMALY_DIG.name = "异常：挖掘"

	Talents.talents_def.T_ANOMALY_ENTOMB.name = "异常：埋葬"

	Talents.talents_def.T_ANOMALY_ENTROPY.name = "异常：熵"

	Talents.talents_def.T_ANOMALY_GRAVITY_WELL.name = "异常：重力井"

	Talents.talents_def.T_ANOMALY_QUAKE.name = "异常：地震"

	Talents.talents_def.T_ANOMALY_FLAWED_DESIGN.name = "异常：不完美设计"

	Talents.talents_def.T_ANOMALY_DUST_STORM.name = "异常：尘土风暴"

	Talents.talents_def.T_ANOMALY_BLAZING_FIRE.name = "异常：燃烧之炎"

	Talents.talents_def.T_ANOMALY_CALCIFY.name = "异常：石化"

	Talents.talents_def.T_ANOMALY_CALL.name = "异常：召唤"

	Talents.talents_def.T_ANOMALY_DEUS_EX.name = "异常：神佑"

	Talents.talents_def.T_ANOMALY_EVIL_TWIN.name = "异常：邪恶双生子"

	Talents.talents_def.T_ANOMALY_INTERSECTING_THREADS.name = "异常：时间线紊乱"

	Talents.talents_def.T_ANOMALY_MASS_DIG.name = "异常：范围挖掘"

	Talents.talents_def.T_ANOMALY_SPHERE_OF_DESTRUCTION.name = "异常：毁灭之球"

	Talents.talents_def.T_ANOMALY_TORNADO.name = "异常：龙卷风"

	Talents.talents_def.T_ANOMALY_METEOR.name = "异常：陨石"

	Talents.talents_def.T_ANOMALY_SPACETIME_TEAR.name = "异常：空间撕裂"

	Talents.talents_def.T_ANOMALY_SUMMON_TIME_ELEMENTAL.name = "异常：召唤时间元素"
