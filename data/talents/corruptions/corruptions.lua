dofile("data-chn123/talents/corruptions/black-magic.lua")
dofile("data-chn123/talents/corruptions/brutality.lua")
dofile("data-chn123/talents/corruptions/demonic-strength.lua")
dofile("data-chn123/talents/corruptions/demon-pact.lua")
dofile("data-chn123/talents/corruptions/demon-seeds.lua")
dofile("data-chn123/talents/corruptions/doom-covenant.lua")
dofile("data-chn123/talents/corruptions/doomshield.lua")
dofile("data-chn123/talents/corruptions/fearfire.lua")
dofile("data-chn123/talents/corruptions/heart-of-fire.lua")
dofile("data-chn123/talents/corruptions/infernal-combat.lua")
dofile("data-chn123/talents/corruptions/npcs.lua")
dofile("data-chn123/talents/corruptions/oppression.lua")
dofile("data-chn123/talents/corruptions/spellblaze.lua")
dofile("data-chn123/talents/corruptions/torture.lua")
dofile("data-chn123/talents/corruptions/wrath.lua")