local Talents = require "engine.interface.ActorTalents"
local damDesc = Talents.main_env.damDesc
local DamageType = require "engine.DamageType"

Talents.talents_def.T_BURNING_HEX.name= "燃烧邪术"

Talents.talents_def.T_PACIFICATION_HEX.name= "宁神邪术"

Talents.talents_def.T_EMPATHIC_HEX.name= "转移邪术"

Talents.talents_def.T_DOMINATION_HEX.name= "支配邪术"


