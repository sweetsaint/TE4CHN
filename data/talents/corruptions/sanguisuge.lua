local Talents = require "engine.interface.ActorTalents"
local damDesc = Talents.main_env.damDesc
local DamageType = require "engine.DamageType"

Talents.talents_def.T_DRAIN.name= "枯萎吸收"

Talents.talents_def.T_BLOODCASTING.name= "血祭施法"

Talents.talents_def.T_ABSORB_LIFE.name= "生命吞噬"

Talents.talents_def.T_LIFE_TAP.name= "生命源泉"
