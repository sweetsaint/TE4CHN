local Talents = require "engine.interface.ActorTalents"
local damDesc = Talents.main_env.damDesc
local DamageType = require "engine.DamageType"

Talents.talents_def.T_TACTICAL_EXPERT.name= "战术大师"
Talents.talents_def.T_TACTICAL_EXPERT.info= function(self, t)
		local defense = t.getDefense(self, t)
		local maximum = t.getMaximum(self, t)
		return ([[每 个 可 见 的 相 邻 敌 人 可 以 使 你 的 闪 避 增 加 %d 点， 最 大 增 加 +%d 点 闪 避。 
		 受 灵 巧 影 响， 闪 避 增 益 和 增 益 最 大 值 按 比 例 加 成。]]):format(defense, maximum)
	end
Talents.talents_def.T_COUNTER_ATTACK.name= "相位反击"
Talents.talents_def.T_COUNTER_ATTACK.info= function(self, t)
		local damage = t.getDamage(self, t) * 100
		return ([[当 你 闪 避 一 次 紧 靠 着 你 的 对 手 的 近 战 攻 击 时 你 有 %d%% 的 概 率 对 对 方 造 成 一 次 %d%% 伤 害 的 反 击 , 每 回 合 最 多 触 发 %0.1f 次。 
		 徒 手 格 斗 时 会 被 视 作 是 攻 击 姿 态（ 如 果 有 的 话） 的 一 种 结 果， 且 会 产 生 额 外 伤 害 加 成。 
		 装 备 武 器 使 用 此 技 能 时 不 产 生 额 外 伤 害。 
		 受 灵 巧 影 响， 反 击 概 率 和 反 击 数 目 有 额 外 加 成。]]):format(t.counterchance(self,t), damage,  t.getCounterAttacks(self, t))
	end
Talents.talents_def.T_SET_UP.name= "致命闪避"
Talents.talents_def.T_SET_UP.info= function(self, t)
		local duration = t.getDuration(self, t)
		local power = t.getPower(self, t)
		local defense = t.getDefense(self, t)
		return ([[增 加 %d 点 闪 避， 持 续 %d 回 合。 当 你 闪 避 1 次 近 战 攻 击 时， 你 可 以 架 起 目 标， 有 %d%% 概 率 使 你 对 目 标 进 行 1 次 暴 击 并 减 少 它 们 %d 点 豁 免。 
		 受 灵 巧 影 响， 效 果 按 比 例 加 成。]])
		:format(defense, duration, power, power)
	end
Talents.talents_def.T_EXPLOIT_WEAKNESS.name= "弱点感知"
Talents.talents_def.T_EXPLOIT_WEAKNESS.info= function(self, t)
		local reduction = t.getReductionMax(self, t)
		return ([[感 知 对 手 的 物 理 弱 点， 代 价 是 你 减 少 10%% 物 理 伤 害。 每 次 你 击 中 对 手 时， 你 会 减 少 它 们 5%% 物 理 伤 害 抵 抗， 最 多 减 少 %d%% 。]]):format(reduction)
	end

