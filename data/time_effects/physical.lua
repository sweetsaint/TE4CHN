﻿timeEffectCHN:newEffect{
	id = "ITEM_ANTIMAGIC_SCOURED",
	enName = "Scoured",
	chName = "冲刷",
	type = "物理",
	subtype = "酸液",
}

timeEffectCHN:newEffect{
	id = "RELENTLESS_TEMPO",
	enName = "Relentless Tempo",
	chName = "无尽节奏",
	type = "物理",
	subtype = "节奏",
}
timeEffectCHN:newEffect{
	id = "DELIRIOUS_CONCUSSION",
	enName = "Concussion",
	chName = "脑震荡",
	type = "物理",
	subtype = "脑震荡",
}


timeEffectCHN:newEffect{
	id = "CUT",
	enName = "Bleeding",
	chName = "流血",
	type = "物理",
	subtype = "创伤/流血",
}

timeEffectCHN:newEffect{
	id = "DEEP_WOUND",
	enName = "Deep Wound",
	chName = "重伤",
	type = "物理",
	subtype = "创伤/流血",
}

timeEffectCHN:newEffect{
	id = "REGENERATION",
	enName = "Regeneration",
	chName = "再生",
	type = "物理",
	subtype = "自然/治疗",
}

timeEffectCHN:newEffect{
	id = "POISONED",
	enName = "Poisoned",
	chName = "中毒",
	type = "物理",
	subtype = "自然/毒素",
}
timeEffectCHN:newEffect{
	id = "POISON",
	enName = "Poison",
	chName = "中毒",
	type = "物理",
	subtype = "自然/毒素",
}

timeEffectCHN:newEffect{
	id = "SPYDRIC_POISON",
	enName = "Spydric Poison",
	chName = "蜘蛛之毒",
	type = "物理",
	subtype = "自然/毒素/定身",
}

timeEffectCHN:newEffect{
	id = "INSIDIOUS_POISON",
	enName = "Insidious Poison",
	chName = "阴险毒药",
	type = "物理",
	subtype = "自然/毒素",
}

timeEffectCHN:newEffect{
	id = "CRIPPLING_POISON",
	enName = "Crippling Poison",
	chName = "致残毒药",
	type = "物理",
	subtype = "自然/毒素",
}

timeEffectCHN:newEffect{
	id = "NUMBING_POISON",
	enName = "Numbing Poison",
	chName = "痳痹毒药",
	type = "物理",
	subtype = "自然/毒素",
}

timeEffectCHN:newEffect{
	id = "STONE_POISON",
	enName = "Stoning Poison",
	chName = "石化毒药",
	type = "物理",
	subtype = "毒素/大地",
}

timeEffectCHN:newEffect{
	id = "BURNING",
	enName = "Burning",
	chName = "燃烧",
	type = "物理",
	subtype = "火焰",
}

timeEffectCHN:newEffect{
	id = "BURNING_SHOCK",
	enName = "Burning Shock",
	chName = "火焰冲击",
	type = "物理",
	subtype = "火焰/震慑",
}

timeEffectCHN:newEffect{
	id = "STUNNED",
	enName = "Stunned",
	chName = "震慑",
	type = "物理",
	subtype = "震慑",
}

timeEffectCHN:newEffect{
	id = "DISARMED",
	enName = "Disarmed",
	chName = "缴械",
	type = "物理",
	subtype = "缴械",
}

timeEffectCHN:newEffect{
	id = "CONSTRICTED",
	enName = "Constricted",
	chName = "扼制",
	type = "物理",
	subtype = "抓取/定身",
}

timeEffectCHN:newEffect{
	id = "DAZED",
	enName = "Dazed",
	chName = "眩晕",
	type = "物理",
	subtype = "震慑",
}

timeEffectCHN:newEffect{
	id = "EVASION",
	enName = "Evasion",
	chName = "闪避",
	type = "物理",
	subtype = "闪避",
}

timeEffectCHN:newEffect{
	id = "SPEED",
	enName = "Speed",
	chName = "加速",
	type = "物理",
	subtype = "加速",
}

timeEffectCHN:newEffect{
	id = "SLOW",
	enName = "Slow",
	chName = "减速",
	type = "物理",
	subtype = "减速",
}

timeEffectCHN:newEffect{
	id = "BLINDED",
	enName = "Blinded",
	chName = "致盲",
	type = "物理",
	subtype = "致盲",
}

timeEffectCHN:newEffect{
	id = "DWARVEN_RESILIENCE",
	enName = "Dwarven Resilience",
	chName = "矮人防御",
	type = "物理",
	subtype = "大地",
}

timeEffectCHN:newEffect{
	id = "STONE_SKIN",
	enName = "Stoneskin",
	chName = "石化皮肤",
	type = "物理",
	subtype = "大地",
}

timeEffectCHN:newEffect{
	id = "THORNY_SKIN",
	enName = "Thorny Skin",
	chName = "荆棘皮肤",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "FROZEN_FEET",
	enName = "Frozen Feet",
	chName = "冻结双脚",
	type = "物理",
	subtype = "寒冰/定身",
}

timeEffectCHN:newEffect{
	id = "FROZEN",
	enName = "Frozen",
	chName = "冰冻",
	type = "物理",
	subtype = "寒冰/震慑",
}

timeEffectCHN:newEffect{
	id = "ETERNAL_WRATH",
	enName = "Wrath of the Eternals",
	chName = "无尽愤怒",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "SHELL_SHIELD",
	enName = "Shell Shield",
	chName = "甲壳护盾",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "PAIN_SUPPRESSION",
	enName = "Pain Suppression",
	chName = "痛苦压制",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "PRIMAL_ATTUNEMENT",
	enName = "Primal Attunement",
	chName = "自然协调",
	type = "物理",
	subtype = "自然",
}


timeEffectCHN:newEffect{
	id = "PURGE_BLIGHT",
	enName = "Purge Blight",
	chName = "枯萎净化",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "SENSE",
	enName = "Sensing",
	chName = "感知",
	type = "物理",
	subtype = "感知",
}

timeEffectCHN:newEffect{
	id = "HEROISM",
	enName = "Heroism",
	chName = "英勇",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "SUNDER_ARMOUR",
	enName = "Sunder Armour",
	chName = "破甲",
	type = "物理",
	subtype = "分解",
}

timeEffectCHN:newEffect{
	id = "SUNDER_ARMS",
	enName = "Sunder Arms",
	chName = "破刃",
	type = "物理",
	subtype = "分解",
}

timeEffectCHN:newEffect{
	id = "PINNED",
	enName = "Pinned to the ground",
	chName = "定身",
	type = "物理",
	subtype = "定身",
}
timeEffectCHN:newEffect{
	id = "BONE_GRAB",
	enName = "Pinned to the ground",
	chName = "定身",
	type = "物理",
	subtype = "定身",
}
timeEffectCHN:newEffect{
	id = "MIGHTY_BLOWS",
	enName = "Mighty Blows",
	chName = "猛力攻击",
	type = "物理",
	subtype = "傀儡",
}

timeEffectCHN:newEffect{
	id = "CRIPPLE",
	enName = "Cripple",
	chName = "致残",
	type = "物理",
	subtype = "创伤",
}

timeEffectCHN:newEffect{
	id = "BURROW",
	enName = "Burrow",
	chName = "穿墙",
	type = "物理",
	subtype = "大地",
}

timeEffectCHN:newEffect{
	id = "DIM_VISION",
	enName = "Reduced Vision",
	chName = "视力下降",
	type = "物理",
	subtype = "感知",
}

timeEffectCHN:newEffect{
	id = "RESOLVE",
	enName = "Resolve",
	chName = "坚韧",
	type = "物理",
	subtype = "自然/反魔法",
}

timeEffectCHN:newEffect{
	id = "WILD_SPEED",
	enName = "Wild Speed",
	chName = "狂暴加速",
	type = "物理",
	subtype = "自然/加速",
}

timeEffectCHN:newEffect{
	id = "STEP_UP",
	enName = "Step Up",
	chName = "步步为营",
	type = "物理",
	subtype = "策略/加速",
}

timeEffectCHN:newEffect{
	id = "LIGHTNING_SPEED",
	enName = "Lightning Speed",
	chName = "闪电加速",
	type = "物理",
	subtype = "闪电/加速",
}

timeEffectCHN:newEffect{
	id = "DRAGONS_FIRE",
	enName = "Dragon's Fire",
	chName = "龙之焰",
	type = "物理",
	subtype = "火焰",
}

timeEffectCHN:newEffect{
	id = "GREATER_WEAPON_FOCUS",
	enName = "Greater Weapon Focus",
	chName = "双重打击",
	type = "物理",
	subtype = "策略",
}

timeEffectCHN:newEffect{
	id = "GRAPPLING",
	enName = "Grappling",
	chName = "抓取",
	type = "物理",
	subtype = "抓取",
}

timeEffectCHN:newEffect{
	id = "GRAPPLED",
	enName = "Grappled",
	chName = "被抓取",
	type = "物理",
	subtype = "抓取/定身",
}

timeEffectCHN:newEffect{
	id = "CRUSHING_HOLD",
	enName = "Crushing Hold",
	chName = "折颈",
	type = "物理",
	subtype = "抓取",
}

timeEffectCHN:newEffect{
	id = "STRANGLE_HOLD",
	enName = "Strangle Hold",
	chName = "扼喉",
	type = "物理",
	subtype = "抓取/沉默",
}

timeEffectCHN:newEffect{
	id = "MAIMED",
	enName = "Maimed",
	chName = "断筋",
	type = "物理",
	subtype = "创伤/减速",
}

timeEffectCHN:newEffect{
	id = "COMBO",
	enName = "Combo",
	chName = "连击",
	type = "物理",
	subtype = "策略",
}

timeEffectCHN:newEffect{
	id = "DEFENSIVE_MANEUVER",
	enName = "Defensive Maneuver",
	chName = "闪避姿态",
	type = "物理",
	subtype = "闪避",
}

timeEffectCHN:newEffect{
	id = "SET_UP",
	enName = "Set Up",
	chName = "失去平衡",
	type = "物理",
	subtype = "策略",
}

timeEffectCHN:newEffect{
	id = "Recovery",
	enName = "Recovery",
	chName = "恢复",
	type = "物理",
	subtype = "治疗",
}

timeEffectCHN:newEffect{
	id = "REFLEXIVE_DODGING",
	enName = "Reflexive Dodging",
	chName = "闪避反射",
	type = "物理",
	subtype = "闪避/加速",
}

timeEffectCHN:newEffect{
	id = "WEAKENED_DEFENSES",
	enName = "Weakened Defenses",
	chName = "碎甲",
	type = "物理",
	subtype = "分解",
}

timeEffectCHN:newEffect{
	id = "STONE_VINE",
	enName = "Stone Vine",
	chName = "石藤",
	type = "物理",
	subtype = "大地/定身",
}

timeEffectCHN:newEffect{
	id = "WATERS_OF_LIFE",
	enName = "Waters of Life",
	chName = "生命之水",
	type = "物理",
	subtype = "自然/治疗",
}

timeEffectCHN:newEffect{
	id = "ELEMENTAL_HARMONY",
	enName = "Elemental Harmony",
	chName = "元素和谐",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "HEALING_NEXUS",
	enName = "Healing Nexus",
	chName = "联结治疗",
	type = "物理",
	subtype = "自然/治疗",
}

timeEffectCHN:newEffect{
	id = "PSIONIC_BIND",
	enName = "Immobilized",
	chName = "固定",
	type = "物理",
	subtype = "念力/震慑",
}

timeEffectCHN:newEffect{
	id = "IMPLODING",
	enName = "Imploding (slow)",
	chName = "碎骨压制（减速）",
	type = "物理",
	subtype = "念力/减速",
}

timeEffectCHN:newEffect{
	id = "FREE_ACTION",
	enName = "Free Action",
	chName = "自由行动",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "ADRENALINE_SURGE",
	enName = "Adrenaline Surge",
	chName = "肾上腺素",
	type = "物理",
	subtype = "狂热",
}

timeEffectCHN:newEffect{
	id = "BLINDSIDE_BONUS",
	enName = "Blindside Bonus",
	chName = "攻其不备",
	type = "物理",
	subtype = "闪避",
}

timeEffectCHN:newEffect{
	id = "OFFBALANCE",
	enName = "Off-balance",
	chName = "失去平衡",
	type = "物理",
	subtype = "多重",
}

timeEffectCHN:newEffect{
	id = "OFFGUARD",
	enName = "Off-guard",
	chName = "猝不及防",
	type = "物理",
	subtype = "多重",
}

timeEffectCHN:newEffect{
	id = "SLOW_MOVE",
	enName = "Slow movement",
	chName = "减速",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "WEAKENED",
	enName = "Weakened",
	chName = "削弱",
	type = "物理",
	subtype = "诅咒",
}

timeEffectCHN:newEffect{
	id = "LOWER_FIRE_RESIST",
	enName = "Lowered fire resistance",
	chName = "火焰抗性下降",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "LOWER_COLD_RESIST",
	enName = "Lowered cold resistance",
	chName = "寒冰抗性下降",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "LOWER_NATURE_RESIST",
	enName = "Lowered nature resistance",
	chName = "自然抗性下降",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "LOWER_PHYSICAL_RESIST",
	enName = "Lowered physical resistance",
	chName = "物理抗性下降",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "CURSED_WOUND",
	enName = "Cursed Wound",
	chName = "诅咒创伤",
	type = "物理",
	subtype = "创伤",
}

timeEffectCHN:newEffect{
	id = "LUMINESCENCE",
	enName = "Luminescence",
	chName = "冷光",
	type = "物理",
	subtype = "自然/光系",
}

timeEffectCHN:newEffect{
	id = "SPELL_DISRUPTION",
	enName = "Spell Disruption",
	chName = "法术干扰",
	type = "物理",
	subtype = "反魔",
}

timeEffectCHN:newEffect{
	id = "RESONANCE",
	enName = "Resonance",
	chName = "共鸣",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "THORN_GRAB",
	enName = "Thorn Grab",
	chName = "荆棘之握",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "LEAVES_COVER",
	enName = "Leaves Cover",
	chName = "叶之守护",
	type = "物理",
	subtype = "自然",
}

timeEffectCHN:newEffect{
	id = "BLOCKING",
	enName = "Blocking",
	chName = "格挡",
	type = "物理",
	subtype = "策略",
}
timeEffectCHN:newEffect{
	id = "BRAWLER_BLOCKING",
	enName = "Blocking",
	chName = "格挡",
	type = "物理",
	subtype = "策略",
}
timeEffectCHN:newEffect{
	id = "DEFENSIVE_GRAPPLING",
	enName = "Grappling Defensively",
	chName = "防御姿态",
	type = "物理",
	subtype = "策略",
}
timeEffectCHN:newEffect{
	id = "COUNTERSTRIKE",
	enName = "Counterstrike",
	chName = "反击",
	type = "物理",
	subtype = "策略",
}

timeEffectCHN:newEffect{
	id = "RAVAGE",
	enName = "Ravage",
	chName = "疯狂扭曲",
	type = "物理",
	subtype = "策略",
}

timeEffectCHN:newEffect{
	id = "DISABLE",
	enName = "Disable",
	chName = "残废",
	type = "物理",
	subtype = "重伤",
}

timeEffectCHN:newEffect{
	id = "ANGUISH",
	enName = "Anguish",
	chName = "痛苦",
	type = "物理",
	subtype = "重伤",
}

timeEffectCHN:newEffect{
	id = "FAST_AS_LIGHTNING",
	enName = "Fast As Lightning",
	chName = "迅如雷电",
	type = "物理",
	subtype = "加速",
}

timeEffectCHN:newEffect{
	id = "ELEMENTAL_SURGE_NATURE",
	enName = "Elemental Surge: Nature",
	chName = "元素涌动：自然",
	type = "物理",
	subtype = "状态",
}

timeEffectCHN:newEffect{
	id = "STEAMROLLER",
	enName = "Steamroller",
	chName = "压路机",
	type = "物理",
	subtype = "状态",
}

timeEffectCHN:newEffect{
	id = "STEAMROLLER_USER",
	enName = "Steamroller",
	chName = "压路机",
	type = "物理",
	subtype = "状态",
}

timeEffectCHN:newEffect{
	id = "SPINE_OF_THE_WORLD",
	enName = "Spine of the World",
	chName = "世纪漩涡",
	type = "物理",
	subtype = "状态",
}

timeEffectCHN:newEffect{
	id = "FUNGAL_BLOOD",
	enName = "Fungal Blood",
	chName = "真菌血液",
	type = "物理",
	subtype = "治疗",
}

timeEffectCHN:newEffect{
	id = "MUCUS",
	enName = "Mucus",
	chName = "粘液",
	type = "物理",
	subtype = "粘液",
}
timeEffectCHN:newEffect{
	id = "CORROSIVE_NATURE",
	enName = "Corrosive Nature",
	chName = "自然腐蚀",
	type = "物理",
	subtype = "自然/酸性",
}
timeEffectCHN:newEffect{
	id = "CORROSIVE_NATURE",
	enName = "Corrosive Nature",
	chName = "自然腐蚀",
	type = "物理",
	subtype = "自然/酸性",
}
timeEffectCHN:newEffect{
	id = "NATURAL_ACID",
	enName = "Natural acid",
	chName = "自然酸化",
	type = "物理",
	subtype = "自然/酸性",
}
timeEffectCHN:newEffect{
	id = "MITOSIS",
	enName = "Mitosis",
	chName = "有丝分裂",
	type = "物理",
	subtype = "状态",
}

timeEffectCHN:newEffect{
	id = "MITOSIS_SWAP",
	enName = "Swap",
	chName = "粘液交换",
	type = "物理",
	subtype = "状态",
}

timeEffectCHN:newEffect{
	id = "CORRODE",
	enName = "Corrode",
	chName = "侵蚀",
	type = "物理",
	subtype = "酸性",
}
timeEffectCHN:newEffect{
	id = "SLIPPERY_MOSS",
	enName = "Slippery Moss",
	chName = "光滑苔藓",
	type = "物理",
	subtype = "自然/苔藓",
}
timeEffectCHN:newEffect{
	id = "JUGGERNAUT",
	enName = "Juggernaut",
	chName = "战场主宰",
	type = "物理",
	subtype = "战斗策略",
}
timeEffectCHN:newEffect{
	id = "COUNTER_ATTACKING",
	enName = "Counter Attacking",
	chName = "反击攻击",
	type = "物理",
	subtype = "战斗策略",
}
timeEffectCHN:newEffect{
	id = "HUNTER_SPEED",
	enName = "Hunter",
	cnName = "猎手",
	type = "物理",
	subtype = "自然/速度",
}
timeEffectCHN:newEffect{
	id = "NATURE_REPLENISHMENT",
	enName = "Natural Replenishment",
	cnName = "自然充能",
	type = "物理",
	subtype = "自然",
}
timeEffectCHN:newEffect{
	id = "BERSERKER_RAGE",
	enName = "Berserker Rage",
	cnName = "狂战之怒",
	type = "物理",
	subtype = "策略",
}
timeEffectCHN:newEffect{
	id = "RELENTLESS_FURY",
	enName = "Relentless Fury",
	cnName = "无尽怒火",
	type = "物理",
	subtype = "策略",
}

timeEffectCHN:newEffect{
	id = "SKIRMISHER_DIRECTED_SPEED",
	enName = "Directed Speed",
	cnName = "定向加速",
	type = "物理",
	subtype = "加速",
}
timeEffectCHN:newEffect{
	id = "SKIRMISHER_STUN_INCREASE",
	enName = "Stun Lengthen",
	cnName = "震慑延长",
	type = "物理",
	subtype = "震慑",
}
timeEffectCHN:newEffect{
	id = "SKIRMISHER_ETERNAL_WARRIOR",
	enName = "Eternel Warrior",
	cnName = "不灭战士",
	type = "物理",
	subtype = "士气",
}
timeEffectCHN:newEffect{
	id = "SKIRMISHER_TACTICAL_POSITION",
	enName = "Tactical Position",
	cnName = "策略走位",
	type = "物理",
	subtype = "策略",
}
timeEffectCHN:newEffect{
	id = "SKIRMISHER_DEFENSIVE_ROLL",
	enName = "Defensive Roll",
	cnName = "防御滚动",
	type = "物理",
	subtype = "策略",
}

timeEffectCHN:newEffect{
	id = "SKIRMISHER_TRAINED_REACTIONS_COOLDOWN",
	enName = "Trained Reactions Cooldown",
	cnName = "训练反应冷却",
	type = "其他",
	subtype = "冷却",
}
timeEffectCHN:newEffect{
	id = "SKIRMISHER_SUPERB_AGILITY",
	enName = "Superb Agility",
	cnName = "异常灵活",
	type = "其他",
	subtype = "冷却",
}

timeEffectCHN:newEffect{
	id = "ANTI_GRAVITY",
	enName = "Anti-Gravity",
	cnName = "反重力",
	type = "物理",
	subtype = "空间",
}