﻿-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2015 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

newAchievement{
	name = "The Arena",
	show = "full",
	desc = [[开启竞技场模式。]],
}

newAchievement{
	name = "Arena Battler 20",
	show = "full",
	desc = [[竞技场生存至第20轮。]],
}

newAchievement{
	name = "Arena Battler 50",
	show = "full",
	desc = [[竞技场生存至第50轮。]],
}

newAchievement{
	name = "Almost Master of Arena",
	show = "full", huge=true,
	desc = [[在30波模式中成为新的竞技场之王。]],
}

newAchievement{
	name = "Master of Arena",
	show = "full", huge=true,
	desc = [[在60波模式中成为新的竞技场之王。]],
}

newAchievement{
	name = "XXX the Destroyer",
	show = "full",
	desc = [[在竞技场中获得“毁灭者”称号。]],
}

newAchievement{
	name = "Grand Master",
	show = "full",
	desc = [[在竞技场中获得“竞技大师”称号。]],
}

newAchievement{
	name = "Ten at one blow",
	show = "full",
	desc = [[在竞技场中用一次攻击杀死10个或更多敌人。]],
}
