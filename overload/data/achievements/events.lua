﻿-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2015 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

newAchievement{
	name = "The sky is falling!", id = "EVENT_METEOR",
	show = "name",
	desc = [[看到一块巨大的陨石从天而降。]],
}

newAchievement{
	name = "Demonic Invasion", id = "EVENT_FEARSCAPE",
	show = "name",
	desc = [[关闭传送门来阻止恶魔入侵。]],
}

newAchievement{
	name = "Invasion from the Depths", id = "EVENT_NAGA",
	show = "name",
	desc = [[关闭传送门来阻止娜迦入侵。]],
}

newAchievement{
	name = "The Restless Dead", id = "EVENT_OLDBATTLEFIELD",
	show = "name",
	desc = [[踏入古战场并最终生还。]],
}

newAchievement{
	name = "The Rat Lich", id = "EVENT_RATLICH",
	show = "name", huge=true,
	desc = [[杀死可怕的巫妖鼠。]],
}

newAchievement{
	name = "Shasshhiy'Kaish", id = "EVENT_CULTISTS",
	show = "name",
	desc = [[在莎西·凯希成长到非常强大时杀死她。]],
}

newAchievement{
	name = "Bringer of Doom", id = "EVENT_PEDESTALS",
	show = "name",
	desc = [[杀死一名厄运行者。]],
}

newAchievement{
	name = "A living one!", id = "CALDIZAR",
	show = "name", huge=true,
	desc = [[传送进凯尔帝勒的堡垒，群星中的虚空地带。]],
}

newAchievement{
	name = "Slimefest", id = "SLUDGENEST100",
	show = "full",
	desc = [[淤泥巢穴里有超过100个墙变成了敌对生物。]],
}

newAchievement{
	name = "Slime killer party", id = "SLUDGENEST200",
	show = "full", huge=true,
	desc = [[淤泥巢穴里有超过200个墙变成了敌对生物。]],
}

newAchievement{
	name = "Mad slime dash", id = "SLUDGENEST300",
	show = "full", huge=true,
	desc = [[淤泥巢穴里有超过300个墙变成了敌对生物。]],
}

newAchievement{
	name = "Don't mind the slimy smell", id = "SLUDGENEST400",
	show = "full", huge=true,
	desc = [[淤泥巢穴里有超过400个墙变成了敌对生物。]],
}

newAchievement{
	name = "In the company of slimes", id = "SLUDGENEST500",
	show = "full", huge=true,
	desc = [[淤泥巢穴里有超过500个墙变成了敌对生物。]],
}
