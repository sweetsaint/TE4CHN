﻿-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009, 2010, 2011, 2012, 2013 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

--------------------------------------------------------------------------
-- Age of Pyre
--------------------------------------------------------------------------

newLore{
	id = "broken-atamathon",
	category = "age of pyre",
	name = "Atamathon, the giant golem",
	lore = [[这尊傀儡之王是由半身人在派尔大战中为对抗兽族制造的，但它却被吞噬者加库尔所摧毁。
它的躯干是用大理石铸造，关节部分则灌入沃瑞坦衔接，双眼由纯净的红宝石构成。它的一只眼睛似乎已经失落。四十多尺高的巨像似乎默默的俯视着你。
一些蠢货似乎想要重塑他，但由于找不到那只眼睛而没有完成。]]
}
