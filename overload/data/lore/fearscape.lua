﻿-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2014 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

--------------------------------------------------------------------------
-- Fearscape, the demon plane
--------------------------------------------------------------------------

newLore{
	id = "fearscape-entry",
	category = "fearscape",
	name = "a fearsome sight",
	lore = [[你所站立之处烈火燎原，火焰如地狱之风般来回舞动。你站在一片火海之中。火焰在前后舞动，像地狱之种随风摇摆。火焰炙烤着你的脚、你的皮肤、你的脸，在血肉中蔓延，爬进你的喉咙。它燃烧着你的身体。狂热的思想强行侵入你的意志，告诉你这里是高达勒斯——恐惧之地。这里不欢迎你的到来。
在远方，你看到阴森的，恶魔形状的黑暗物质笼罩在这燃烧的大地上，如同毁灭守卫一般。漆黑的天空中悬挂着一个圆形的，是被你称之为埃亚尔的世界……在这燃烧着的平原上聚集了大批的恶魔战士，他们的瞳孔中充满了对你故乡的憎恨。突然间，你和它都显得如此渺小而脆弱。当火焰将痛苦注满你的身体时，你唯一的想法就是——逃走。]],
}

newLore{
	id = "kryl-feijan-altar",
	category = "fearscape",
	name = "sacrificial altar", always_pop = true,
	image = "kryl_feijan_sacrificial_altar.png",
	lore = [[你发现一名人类女子躺在一座黑色的祭坛上昏迷不醒，扭曲的魔法铭文深深的烙入了她赤裸的身体。
在她周围你发现了数名黑袍人。

当他们发现你时，其中一人大声呼喊“入侵者！我们必须保护卡洛·斐济之种！”]],
}
