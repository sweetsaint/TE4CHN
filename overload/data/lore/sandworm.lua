﻿-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009, 2010, 2011, 2012, 2013 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

--------------------------------------------------------------------------
-- Sandworm Lair
--------------------------------------------------------------------------

newLore{
	id = "sandworm-note-1",
	category = "sandworm lair",
	name = "song of the sands",
	lore = [[我已经跟踪赤龙数月
巨龙的狡诈隐匿它的踪迹
当我穿过沙漠，领略沙暴
我找到了一些其他的东西

我走在巨大沙虫的足迹中
通过布满沙子的地下通道
奥术力量让那里无法交谈
我正朝着那龙人之路前进]],
}
newLore{
	id = "sandworm-note-2",
	category = "sandworm lair",
	name = "song of the sands",
	lore = [[巨龙吐息腐蚀了我的双眼
撕裂了我的身体
但我仍然坚定的前进
去看那坐落彼端的它
在那无尽沙堆的深处

那些沙柱从我头边擦过
差点把我活埋
但是匆忙中我竟忘记了恐惧
为了任务我一定要活着！]],
}
newLore{
	id = "sandworm-note-3",
	category = "sandworm lair",
	name = "song of the sands",
	lore = [[这沙虫变得越来越强大
似乎比它们以往更巨大
无法描述的高与宽
好像是做梦一般……

我现在正准备启程前往黑暗深处
寻找属于我自己的命运
我是多么的渴望尝到那跳动的心脏
传说沙虫跳动的心脏美味无比！]],
}
newLore{
	id = "sandworm-note-4",
	category = "sandworm lair",
	name = "song of the sands",
	lore = [[我看到了真实的女王！
她深深打动了我的灵魂
啊，女王！请让我和你融为一体！
请吞噬我！将我整个都吞下去吧！！]],
}
