﻿-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009, 2010, 2011, 2012, 2013 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

return [[Welcome #LIGHT_GREEN#@name@#WHITE#.
 你 是 令 人 望 风 丧 胆 的 兽 人 种 族 的 一 员。 
BLAH BLAH BLAH

 你 被 派 往 远 东 大 陆 西 南 边 的 一 个 最 近 的 陆 地， 去 摧 毁 太 阳 之 墙 的 一 个 前 哨 站 — — 这 个 大 陆 幸 存 的 最 后 一 批 人 类、 精 灵 和 矮 人 的 聚 居 地。 

 南 方 去 一 点 点 就 是 那 个 前 哨 战。 你 的 目 标： 烧 光、 抢 光、 杀 光！ 
]]