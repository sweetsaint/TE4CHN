-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2014 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Nicolas Casalini "DarkGod"
-- darkgod@te4.org

return "New Race: #LIGHT_GREEN#Doomelf",
[[Doomelves are not a real race, they are Shaloren that have been taken by demons and transformed into harbingers of doom.
They skills in inflicting and resisting pain has been honed by their rigorous training on the Fearscape.

You have killed the only three explorers from Mal'Rok that could have told the demons the truth and thus have earned the right to make #LIGHT_GREEN#Doomelf#WHITE# characters.

Race features:#YELLOW#
- Instant cast phase door
- Can turn into a dúathedlen
- Can increase detrimental effects and reduce beneficial ones on their foes
#WHITE#
]]
